//
//  Err.swift
//  Posta
//
//  Created by Dan Rudolf on 6/14/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation

struct Err : Codable {
    let code : Int
    let message : String

    init(code: Int, message: String) {
        self.code = code
        self.message = message
    }

    init(type: FailType) {
        switch type {
        case FailType.BadResponse:
            self.code = 500
            self.message = "Something went wrong processing your request, please try again."
            break
        case FailType.NetworkError:
            self.code = 500
            self.message = "Unable to complete your request. Please try again."
            break
        default:
            self.code = 500
            self.message = "Something went wrong processing your request."
            break
        }
    }
    enum FailType {
        case BadResponse, MalformedData, NetworkError
    }
}
