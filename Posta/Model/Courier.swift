//
//  Courier.swift
//  Posta
//
//  Created by Dan Rudolf on 9/12/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation
import CoreLocation

struct Courier : Codable {
    var id: String
    var location: AnnonLocation
    var name: Name
    var phone: String
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case location = "location"
        case phone
        case name
    }
}

struct CourierLocationRes : Codable {
    let success : Bool
    let error : Err?
    let locations : [CourierLocation]?
}


struct CourierLocation: Codable {
    var id: String?
    var location: AnnonLocation?
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case location = "location"
    }
}
struct AnnonLocation: Codable {
    var coordinates: Array<Double>?
}

extension AnnonLocation {
    func getCoordinates() -> CLLocationCoordinate2D{
        return CLLocationCoordinate2D.init(latitude: self.coordinates![1], longitude: self.coordinates![0])
    }
}
