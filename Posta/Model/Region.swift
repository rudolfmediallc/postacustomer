//
//  Region.swift
//  Posta
//
//  Created by Dan Rudolf on 6/21/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation
import CoreLocation

struct RegionRes : Codable {
    let success : Bool
    let error : Err?
    let region : Region?
}

struct Region : Codable{
    let bounds: Bounds
}

struct Bounds : Codable{
    let maxLat: Double = 41.904060
    let maxLon: Double = -87.656727
    let minLat: Double = 41.866917
    let minLon: Double = -87.613461
    enum CodingKeys: String, CodingKey {
        case maxLat = "max_lat"
        case maxLon = "max_lon"
        case minLat = "min_lat"
        case minLon = "min_lon"
    }

    func getDelieryZoneCoordinates() -> [CLLocationCoordinate2D]{
        return [
            CLLocationCoordinate2D.init(latitude: 41.903580, longitude: -87.656727),
            CLLocationCoordinate2D.init(latitude: 41.904060, longitude: -87.625107),
            CLLocationCoordinate2D.init(latitude: 41.892145, longitude: -87.609022),
            CLLocationCoordinate2D.init(latitude: 41.887190, longitude: -87.612856),
            CLLocationCoordinate2D.init(latitude: 41.867368, longitude: -87.618899),
            CLLocationCoordinate2D.init(latitude: 41.866917, longitude: -87.656727),
        ]
    }

    func checkIsInRegion(point: CLLocationCoordinate2D) -> Bool{
        guard point.latitude.magnitude > minLat.magnitude else {
            return false
        }
        guard point.latitude.magnitude < maxLat.magnitude else {
            return false
        }
        guard point.longitude.magnitude > minLon.magnitude else {
            return false
        }
        guard point.longitude.magnitude < maxLon.magnitude else{
            return false
        }
        return true
    }

}
