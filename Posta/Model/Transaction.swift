//
//  Transaction.swift
//  Posta
//
//  Created by Dan Rudolf on 10/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation

struct Transaction : Codable {
    var id: String
    var amount: Amount
    var charge: Charge

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case amount
        case charge
    }
}

struct Amount : Codable {
    var total: Int
    var fees: Int
    var destinationAmount: Int
    var withheld: Int

    enum CodingKeys: String, CodingKey {
        case total = "total_charge"
        case fees = "total_fees"
        case destinationAmount = "destination_amount"
        case withheld = "withheld"
    }

}

struct Charge : Codable {
    var last4: String
    var cardType: String

    enum CodingKeys: String, CodingKey {
        case cardType = "card_type"
        case last4
    }
}
