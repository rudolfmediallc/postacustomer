//
//  Card.swift
//  Posta
//
//  Created by Dan Rudolf on 9/17/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation
import UIKit

struct CardRes: Codable {
    let success : Bool
    let error : Err?
    let card : Card?
}

struct Card : Codable{
    var addressZip: String
    var brand: String
    var lastFour: String
    var expMonth: Int
    var extYear: Int
    enum CodingKeys: String, CodingKey {
        case addressZip = "address_zip"
        case brand
        case lastFour = "last4"
        case expMonth = "exp_month"
        case extYear = "exp_year"
    }

    func getCardImage() -> UIImage{
        switch self.brand {
        case "American Express":
            return UIImage.init(named: "amex_small")!
        case "Diners Club":
            return UIImage.init(named: "dc_small")!
        case "Discover":
            return UIImage.init(named: "discover_small")!
        case "JCB":
            return UIImage.init(named: "jcb_small")!
        case "MasterCard":
            return UIImage.init(named: "master_card_small")!
        case "UnionPay":
            return UIImage.init(named: "union_pay_small")!
        case "Visa":
            return UIImage.init(named: "visa_small")!
        default:
            return UIImage.init(named: "default_card_small")!
        }
    }
}

