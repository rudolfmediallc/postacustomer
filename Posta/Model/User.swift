//
//  User.swift
//  Posta
//
//  Created by Dan Rudolf on 6/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation
import CoreLocation

struct RecoveryRes : Codable {
    let success : Bool
    let error : Err?
    let deviceKey : String?

    enum CodingKeys: String, CodingKey {
        case deviceKey = "device_key"
        case success
        case error
    }
}

struct ConfrimDeviceRes : Codable {
    let success : Bool
    let error : Err?
    let resetKey : String?

    enum CodingKeys: String, CodingKey {
        case resetKey = "device_confirmed"
        case success
        case error
    }
}

struct PasswordResetRes : Codable {
    let success : Bool
    let error : Err?
    let password_updated : Bool?
}

struct UserRes : Codable {
    let success : Bool
    let error : Err?
    let user : User?
}

struct SenderRes : Codable {
    let success : Bool
    let error : Err?
    let sender : Sender?
}

struct User: Codable {
    var id: String
    var email: String
    var name: Name
    var phone: String
    var device: Device?
    var profile: Sender
    var stripeId: String?
    var defaultPayment: String?
    var currentCard: Card?
    var terms: TermsStatus
    var joinedDate: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case email
        case name
        case phone
        case device
        case profile
        case stripeId = "stripe_id"
        case defaultPayment = "default_payment"
        case terms
        case joinedDate = "created_at"
    }
}



struct Name: Codable {
    var first: String
    var last: String
}

extension Name {
    func getNameString() -> String{
        return "\(first) \(last)"
    }
    
    func getNameAndInitial() -> String{
        if (!last.isEmpty) {
            return "\(first) \(last.first!)"
        }
        return "\(first)"
    }
}

struct Address: Codable {
    var street: String
    var city: String
    var state: String
    var zip: String
    init(street: String, city: String, state: String, zip: String) {
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip
    }
}

extension Address {
    enum Length {
        case short
        case full
    }
    func getStreet(type: Length) -> String{
        return street
    }
    func getLocaleAddress(type: Length) -> String{
        switch (type) {
        case .full:
            return "\(city), \(state) \(zip)"
        case .short:
            return "\(city), \(zip)"
        }
    }
}
struct Sender: Codable {
    var id: String
    var name: Name
    var phone: String
    var address: Address
    var location: Location
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case phone
        case address
        case location
    }
}

struct Location: Codable {
    var coordinates: Array<Double>?
}

extension Location {
    func getCoordinates() -> CLLocationCoordinate2D{
        return CLLocationCoordinate2D.init(latitude: self.coordinates![1], longitude: self.coordinates![0])
    }
    mutating func setCoordinnates(coordinates: CLLocationCoordinate2D){
        let locationCord = [Double(coordinates.longitude), Double(coordinates.latitude)]
        self.coordinates = locationCord
    }
}

struct Device: Codable {
    var os: String?
    var type: Name?
    var id: String?
    var appVersion: String?
    enum CodingKeys: String, CodingKey {
        case os
        case type
        case id
        case appVersion = "app_version"
    }
}

struct TermsStatus: Codable {
    var version: Int?
    var agreedOn: String?
    enum CodingKeys: String, CodingKey {
        case version
        case agreedOn = "agreed_on"
    }
}

struct NewUser : Codable{
    var name: Name
    var email: String
    var phone: String
    var password: String
    
    init(firstName: String, lastName: String, email: String, phone: String, pass: String) {
        self.name = Name.init(first: firstName, last: lastName)
        self.email = email
        self.phone = phone
        self.password = pass
    }
}
