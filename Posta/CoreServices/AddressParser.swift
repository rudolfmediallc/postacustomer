//
//  AddressParser.swift
//  Posta
//
//  Created by Dan Rudolf on 10/23/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation
import MapboxGeocoder

class AddressParser: NSObject {

    let stateNamesHash = States().namesHash

    func parseAddress(placemark: GeocodedPlacemark) -> (address: Address?, coordinate: CLLocationCoordinate2D?){
        guard let address = parsePostalAddress(addressDictionary: placemark.addressDictionary!) else{
            return (nil, nil)
        }
        guard let coordinate = placemark.location?.coordinate else{
            return (nil, nil)
        }
        return (address, coordinate)
    }

    func parsePostalAddress(addressDictionary: [AnyHashable: Any]) -> Address?{
        guard var state = addressDictionary["state"] as? String else{
            return nil
        }
        state = getStateAbreviation(long: state)

        guard let zip = addressDictionary["postalCode"] as? String else{
            return nil
        }

        guard let city = addressDictionary["city"] as? String else {
            return nil
        }

        var street = String()
        if let name = addressDictionary["name"]{
            street = "\(name)"
        }

        if let number = addressDictionary["subThoroughfare"]{
            street = "\(number) \(street)"
        }

        guard !street.isEmpty else{
            return nil
        }

        return Address.init(street: street, city: city, state: state, zip: zip)
    }

    func getStateAbreviation(long: String) -> String {
        guard let abrev = self.stateNamesHash[long] else{
            return long
        }
        return abrev
    }
}
