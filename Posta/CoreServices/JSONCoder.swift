//
//  JSONCoder.swift
//  Posta
//
//  Created by Dan Rudolf on 10/27/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation

class JSONCoder : NSObject{

    func decodeJSON <T : Codable> (responseData: Data, formatStruct: T.Type) -> Any {
        let decoder = JSONDecoder()
        do {
            let decoded = try decoder.decode(formatStruct.self, from: responseData)
            let success = decoded["success"] as? Bool
            if (success != nil && success == true){
                return decoded
            }
            let errJSON = decoded["error"]
            guard let err = errJSON as? Dictionary<AnyHashable, Any> else {
                return Err.init(type: Err.FailType.BadResponse)
            }
            return Err.init(code: err["code"] as! Int, message: err["message"] as! String)
        } catch {
            print("\(error)")
            return Err.init(type: Err.FailType.BadResponse)
        }
    }

    func encodeJSON<T : Codable>(codable: T) -> [AnyHashable : Any]? {
        let encoder = JSONEncoder()
        do {
            let encodedData = try encoder.encode(codable)
            let jsonData = try JSONSerialization.jsonObject(with: encodedData, options: .allowFragments)
            guard let json = jsonData as? [AnyHashable: Any] else{
                return nil
            }
            return json
        } catch {
            return nil
        }
    }
}
