//
//  LocationController.swift
//  Posta
//
//  Created by Dan Rudolf on 10/24/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import CoreLocation
import MapboxGeocoder

struct GeoResult {
    var address: Address
    var coordinate: CLLocationCoordinate2D
}

protocol GeoQueryStremDelegate {
    func didUpdateStreamResults(geoResults: [GeoResult])
}


class GeoController: NSObject, CLLocationManagerDelegate {

    static let sharedInstace = GeoController()

    let geocoder = Geocoder(accessToken: "pk.eyJ1IjoicG9zdGF0ZWFtIiwiYSI6ImNqb29jbHdsYjBteW4zdmx3Mm1kOTU1NHEifQ.WRbUIyJfK37a3BxPDXqCCw")
    let manager = CLLocationManager()
    let addressParser = AddressParser()

    var lastLocation: CLLocation! {
        didSet{
            lastSet = Date.init()
        }
    }
    var geoResults: [GeoResult] = []
    var queryStreamDelegate: GeoQueryStremDelegate?
    var lastSet: Date = Date.init()

    //LocationManger
    func locate(completionHandler: @escaping (CLLocation?) -> Void){
        startUpdatingLocation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            self.manager.stopUpdatingLocation()
            completionHandler(self.lastLocation)
        }
    }

    func startUpdatingLocation(){
        manager.requestWhenInUseAuthorization()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else{
            return
        }
        lastLocation = location
        print("Location upated \(location.horizontalAccuracy) \(location.verticalAccuracy)")
    }

    //GeoCoder
    func reverseGeocodeCurrent(coordinate: CLLocationCoordinate2D, completionHandler: @escaping (Address?, Err?) ->Void){
        let options = ReverseGeocodeOptions(coordinate: coordinate)
        _ = geocoder.geocode(options) { (placemarks, attribution, error) in
            guard let placemark = placemarks?.first else {
                completionHandler(nil,  Err.init(type: Err.FailType.BadResponse))
                return
            }
            let parsed = self.addressParser.parseAddress(placemark: placemark)
            guard let address = parsed.address else{
                completionHandler(nil,  Err.init(type: Err.FailType.MalformedData))
                return
            }
            completionHandler(address, nil)
        }
    }

    func codeStreamQuery(str : String){
        let options = ForwardGeocodeOptions(query: str)
        options.autocompletesQuery = true
        options.allowedISOCountryCodes = ["US"]
        options.allowedScopes = [.address, .pointOfInterest]
        options.focalLocation = CLLocation.init(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude)
        options.allowedRegion = getCoderRegion()

        _ = geocoder.geocode(options) { (placemarks, attribution, error) in
            guard let geoArray = placemarks else {
                return
            }
            self.geoResults.removeAll()
            for placemark in geoArray{
                guard placemark.addressDictionary != nil else {
                    return
                }
                guard placemark.location?.coordinate != nil else {
                    return
                }
                let parsed = self.addressParser.parseAddress(placemark: placemark)
                guard let address = parsed.address, let coordinates = parsed.coordinate else{
                    return
                }
                self.geoResults.append(GeoResult.init(address: address, coordinate: coordinates))
            }
            DispatchQueue.main.async {
                self.queryStreamDelegate?.didUpdateStreamResults(geoResults: self.geoResults)
            }
        }
    }

    func getCoderRegion() -> RectangularRegion{
        let swLat = lastLocation.coordinate.latitude - 0.5
        let swLon = lastLocation.coordinate.longitude - 0.5
        let neLat = lastLocation.coordinate.latitude + 0.5
        let neLon = lastLocation.coordinate.longitude + 0.5

        let southWest = CLLocationCoordinate2D.init(latitude: swLat, longitude: swLon)
        let northEast = CLLocationCoordinate2D.init(latitude: neLat, longitude: neLon)
        return RectangularRegion.init(southWest: southWest, northEast: northEast)
    }

    func clearQueryStreamResults(){
        self.geoResults.removeAll()
    }
}
