//
//  RestController.swift
//  Posta
//
//  Created by Dan Rudolf.
//  Copyright © 2018 Posta Technologies, Inc. All rights reserved.
//

import UIKit

enum Env {
    case testLocal
    case production
    case testRemote
}


enum SessionLength {
    case defaultDuration
    case extendedDuration
}

private struct Host {
    let testLocal = URL.init(string: "http://10.1.10.18:3000")!
    let testRemote = URL.init(string: "http://35.202.245.42:80")!
    let production = URL.init(string: "https://posta.systems")!
}

class RestService: NSObject {

    let authController = AuthController()
    let kAuth = "authorization"
    let coder = JSONCoder()

    var request: URLRequest
    var methods = Methods.init()
    var routes = Routes.init()


    init(environment: Env) {
        switch (environment ){
        case .production:
            request = URLRequest.init(url: Host().production)
            break
        case .testLocal:
            request = URLRequest.init(url: Host().testLocal)
            break
        case .testRemote:
            request = URLRequest.init(url: Host().testRemote)
            break
        }
    }
    
    func getDataTaskSession(timeout: SessionLength) -> URLSession{
        let settings = URLSessionConfiguration.default
        settings.waitsForConnectivity = false

        switch timeout {
        case .defaultDuration:
            settings.timeoutIntervalForRequest = 15
        case .extendedDuration:
            settings.timeoutIntervalForRequest = 30
        }
        return URLSession.shared
    }

    func buildRequest() -> URLRequest{
        return self.request
    }


    func setPath(pathString: String){
        request.url?.appendPathComponent(pathString)
        guard pathString != routes.post.loginV1 else {
            return
        }
        request.setValue(authController.getCurrentToken(), forHTTPHeaderField: kAuth);
    }

    func appendQueryParams(params:[String : String]){
        var components = URLComponents.init(url: self.request.url!, resolvingAgainstBaseURL: true)
        var queryItems : Array<URLQueryItem> = Array.init()
        print(params)
        for p in params{
            let item = URLQueryItem.init(name: p.key, value: p.value)
            queryItems.append(item)
        }
        components?.queryItems = queryItems
        request.url = components?.url
    }
    func appendPathParams(params: String){
        var components = URLComponents.init(url: self.request.url!, resolvingAgainstBaseURL: true)
        components?.path.append(params)
        request.url = components?.url
    }

    func setMethod(method: String){
        request.httpMethod = method
    }

    func setBody(dictionary: [AnyHashable: Any?]) {
        do { let data = try JSONSerialization.data(withJSONObject: dictionary) as NSData
            request.httpBody = data as Data
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("\(data.length)", forHTTPHeaderField: "Content-Length")
        } catch {
            print("Couldnot serialize privided body")
        }
    }

    func parseDataTaskResponse(data: Data?, response: URLResponse?, err: Error?) -> Any {
        guard data != nil else {
            return Err.init(type: Err.FailType.BadResponse)
        }
        guard err == nil else {
            return Err.init(type: Err.FailType.NetworkError)
        }
        return data!
    }


}

extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary[key]
    }
    var data: Data {
        return try! JSONEncoder().encode(self)
    }
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: data)) as? [String: Any] ?? [:]
    }
}
