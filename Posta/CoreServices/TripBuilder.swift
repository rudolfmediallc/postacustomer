//
//  TripBuilder.swift
//  Posta
//
//  Created by Dan Rudolf on 6/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import MapboxGeocoder

protocol TripBuilderDelegate {
    func didSetPickup(address: Address, coordinates: CLLocationCoordinate2D)
    func didSetDropoff(tripRequest: TripRequest)
    func didSetDetails(tripRequest: TripRequest)
    func didSetInstructions(tripRequest: TripRequest)
}


class TripBuilder: NSObject {

    static let sharedInstance = TripBuilder()

    let userController = UserController.sharedInstace
    let region = Region.init(bounds: Bounds.init())
    let addressParser = AddressParser()

    var currentTrip = TripRequest.init(senderId: UserController.currentUser.profile.id)
    var builderDelegate: TripBuilderDelegate?

    func setPickupAddress(newAddress: Address, coordinates: CLLocationCoordinate2D){
        UserController.currentUser.profile.address = newAddress
        UserController.currentUser.profile.location.setCoordinnates(coordinates: coordinates)
        userController.lastLocation = coordinates
        self.builderDelegate?.didSetPickup(address: newAddress, coordinates: coordinates)
    }

    func setDestinationAddress(newAddress : Address, coordinates: CLLocationCoordinate2D){
        let destination = RequestedDestination.init(name: nil, phone: nil, address: newAddress)
        currentTrip.destination = destination
        currentTrip.destintationGeo = DestinationGeo.init()
        currentTrip.destintationGeo?.setCoordinnates(coordinates: coordinates)
        self.builderDelegate?.didSetDropoff(tripRequest: currentTrip)
    }

    func setReceipientDetails(name: Name, phone: String, description: String?){
        currentTrip.destination?.name = name
        currentTrip.destination?.phone = phone
        currentTrip.description = description
        self.builderDelegate?.didSetDetails(tripRequest: currentTrip)
    }

    func setInstructions(instructions: String){
        currentTrip.instructions = instructions
        self.builderDelegate?.didSetInstructions(tripRequest: currentTrip)
    }

    func validate()-> String?{
        guard currentTrip.destination != nil, currentTrip.destintationGeo != nil else{
            return "Please enter a destination for your package."
        }
        return nil
    }

    func clean(){
        self.currentTrip = TripRequest.init(senderId: UserController.currentUser.profile.id)
    }
}
