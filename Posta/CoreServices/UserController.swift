//
//  UserController.swift
//  Posta
//
//  Created by Dan Rudolf on 6/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import CoreLocation

protocol ProfileSyncDelegate {
    func didUpdateProfileData()
}

class UserController: NSObject {
    
    let env = Env.production

    let authController = AuthController.sharedInstance
    static let sharedInstace = UserController()

    static var currentUser: User!
    var userRegion: Region?
    var lastLocation: CLLocationCoordinate2D?
    var profileSyncDelegates = [ProfileSyncDelegate]()

    func syncProfileDetailsViews(){
        DispatchQueue.main.async {
            self.profileSyncDelegates.forEach({ (delegate) in
                delegate.didUpdateProfileData()
            })
        }
    }
    
    func addDelegate(delegate: AnyObject?){
        self.profileSyncDelegates.append(delegate as! ProfileSyncDelegate)
    }

    func removeDelegate(delegate: AnyObject){
        profileSyncDelegates = profileSyncDelegates.filter { type(of: $0) != type(of: delegate) }
    }

    func loginUser(user: String, password: String, completionHandler: @escaping (User?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        restService.setBody(dictionary: ["email" : user, "password" : password])
        restService.setMethod(method: restService.methods.POST)
        restService.setPath(pathString: restService.routes.post.loginV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let res = response as! HTTPURLResponse
            let token = res.allHeaderFields["Authorization"] as! String
            self.authController.setNewToken(token: token)
            let userRes = serializerResponse as! UserRes
            completionHandler(userRes.user, nil)
            }.resume()
    }

    func createUser(user: NewUser, completionHandler: @escaping (User?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        let json = restService.coder.encodeJSON(codable: user)!
        restService.setBody(dictionary: json)
        restService.setMethod(method: restService.methods.POST)
        restService.setPath(pathString: restService.routes.post.newUserV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let res = response as! HTTPURLResponse
            let token = res.allHeaderFields["Authorization"] as! String
            self.authController.setNewToken(token: token)
            let userRes = serializerResponse as! UserRes
            completionHandler(userRes.user, nil)
            }.resume()
    }

    func refreshUserData(completionHandler: @escaping (Bool) -> Void) {
        let restService = RestService.init(environment: env)
        let authController = AuthController.sharedInstance
        restService.setBody(dictionary: ["email" : authController.getCurrentUserName()!, "password" : authController.getCurrentPassword()!])
        restService.setMethod(method: restService.methods.POST)
        restService.setPath(pathString: restService.routes.post.loginV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(false)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserRes.self)
            if serializerResponse is Err {
                completionHandler(false)
                return
            }
            let userRes = serializerResponse as! UserRes
            let res = response as! HTTPURLResponse
            let token = res.allHeaderFields["Authorization"] as! String
            self.authController.setNewToken(token: token)
            UserController.currentUser = userRes.user!
            self.syncProfileDetailsViews()
            completionHandler(true)
            }.resume()
    }

    func updateCurrentLocation(sender: [AnyHashable: Any], completionHandler: @escaping (Sender?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        restService.setBody(dictionary: sender)
        restService.setMethod(method: restService.methods.PUT)
        restService.setPath(pathString: restService.routes.put.updateLocationV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: SenderRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let senderRes = serializerResponse as! SenderRes
            completionHandler(senderRes.sender, nil)
            }.resume()
    }

    func getUserRegion(completionHandler: @escaping (Region?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        restService.setMethod(method: restService.methods.GET)
        restService.setPath(pathString: restService.routes.get.regionV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: RegionRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let regionRes = serializerResponse as! RegionRes
            completionHandler(regionRes.region, nil)
            }.resume()
    }

    func updateUsersName(name: Name, completionHandler: @escaping (Bool?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        guard var body = restService.coder.encodeJSON(codable: name) else{
            completionHandler(false, Err.init(type: .MalformedData))
            return
        }
        body["type"] = "name"
        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.PUT)
        restService.setPath(pathString: restService.routes.put.updateUserV1)
        restService.appendPathParams(params: UserController.currentUser.id)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            self.refreshUserData(completionHandler: { (success) in
                if (success == true){
                    completionHandler(true, nil)
                } else{
                    completionHandler(false, Err.init(code: 500, message: "Unable to update profile Information, please try again."))
                }
            })
            }.resume()
    }

    func updateUsersPhone(phone: String, completionHandler: @escaping (Bool?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        let body = ["type": "phone", "phone": phone];
        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.PUT)
        restService.setPath(pathString: restService.routes.put.updateUserV1)
        restService.appendPathParams(params: UserController.currentUser.id)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            self.refreshUserData(completionHandler: { (success) in
                if (success == true){
                    completionHandler(true, nil)
                } else{
                    completionHandler(false, Err.init(code: 500, message: "Unable to update profile Information, please try again."))
                }
            })
            }.resume()
    }

    func updateUsersEmail(email: String, completionHandler: @escaping (Bool?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        let body = ["type": "email", "email": email];
        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.PUT)
        restService.setPath(pathString: restService.routes.put.updateUserV1)
        restService.appendPathParams(params: UserController.currentUser.id)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let userRes = serializerResponse as! UserRes
            self.authController.setNewUsername(username: userRes.user!.email)
            self.refreshUserData(completionHandler: { (success) in
                if (success == true){
                    completionHandler(true, nil)
                } else{
                    completionHandler(false, Err.init(code: 500, message: "Unable to update profile Information, please try again."))
                }
            })
            }.resume()
    }

    func updateUsersPass(current: String, new: String, completionHandler: @escaping (Bool?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        var body = ["current" : current, "new": new]
        body["type"] = "password"
        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.POST)
        restService.setPath(pathString: restService.routes.post.updatePasswordV1)
        restService.appendPathParams(params: UserController.currentUser.id)
        let req = restService.buildRequest()
        let session = restService.getDataTaskSession(timeout: .defaultDuration)
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            _  = serializerResponse as! UserRes
            self.authController.setNewPassword(password: new)
            self.refreshUserData(completionHandler: { (success) in
                if (success == true){
                    completionHandler(true, nil)
                } else{
                    completionHandler(false, Err.init(code: 500, message: "Unable to update profile Information, please try again."))
                }
            })
            }.resume()
    }

    func agreeToTermsAndConditions(version: Int, completionHandler: @escaping (Bool?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        let body = ["terms_version": version];
        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.PUT)
        restService.setPath(pathString: restService.routes.put.updateTermsAgreementV1)
        restService.appendPathParams(params: UserController.currentUser.id)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let userRes = serializerResponse as! UserRes
            UserController.currentUser = userRes.user!
            completionHandler(true, nil)
            }.resume()
    }

    func requestDeviceKey(phone: String, completionHandler: @escaping (RecoveryRes?, Err?) -> Void) {
        let restService = RestService.init(environment: env)

        let body = ["acct_type" : "SENDER",
                     "phone" : phone,
                     "key" : authController.kReset]

        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.POST)
        restService.setPath(pathString: restService.routes.post.requestDeviceKeyV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: RecoveryRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let recoveryRes = serializerResponse as! RecoveryRes
            print(recoveryRes)
            completionHandler(recoveryRes, nil)
            }.resume()
    }

    func confirmDevice(phone: String, deviceKey: String, confirmCode: String, completionHandler: @escaping (ConfrimDeviceRes?, Err?) -> Void) {
        let restService = RestService.init(environment: env)

        let body = ["device_key" : deviceKey,
                    "confirm_code" : confirmCode,
                    "phone" : phone,
                    "acct_type" : "SENDER"]

        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.POST)
        restService.setPath(pathString: restService.routes.post.confrimDeviceV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: ConfrimDeviceRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let deviceRes = serializerResponse as! ConfrimDeviceRes
            completionHandler(deviceRes, nil)
            }.resume()
    }

    func resetPassword(phone: String, resetKey: String, newPassword: String, completionHandler: @escaping (Bool?, Err?) -> Void) {
        let restService = RestService.init(environment: env)

        let body = ["reset_key" : resetKey,
                    "new_pass" : newPassword,
                    "phone" : phone,
                    "acct_type" : "SENDER"]

        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.POST)
        restService.setPath(pathString: restService.routes.post.resetPasswordV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: PasswordResetRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let resetRes = serializerResponse as! PasswordResetRes
            completionHandler(resetRes.password_updated, nil)
            }.resume()
    }
}
