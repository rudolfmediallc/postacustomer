//
//  ApplicationStateController.swift
//  Posta
//
//  Created by Dan Rudolf on 10/24/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class ApplicationStateController: UIViewController {

    private var currentVC: UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        presentApplicationLoader()
    }

    func presentApplicationLoader(){
        let newRoot = ApplicationLoader.init(nibName: "ApplicationLoader", bundle: Bundle.main)
        addChildViewController(newRoot)
        newRoot.view.frame = view.bounds
        view.addSubview(newRoot.view)
        newRoot.didMove(toParentViewController: self)
        if currentVC != nil{
            currentVC.willMove(toParentViewController: nil)
            currentVC.view.removeFromSuperview()
            currentVC.removeFromParentViewController()
        }
        currentVC = newRoot
    }

    func presentLogin(){
        let newRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LOGIN_VC") as! LoginVC
        animateFadeTransition(to: newRoot)
    }

    func presentApplicationRoot(){
        let newRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "APPLICATION_ROOT") as! MainVC
        animateFadeTransition(to: newRoot)
    }
    

    private func animateFadeTransition(to destination: UIViewController, completion: (() -> Void)? = nil) {
        currentVC.willMove(toParentViewController: nil)
        addChildViewController(destination)
        transition(from: currentVC, to: destination, duration: 0.3, options: [.transitionCrossDissolve, .curveEaseOut], animations: {
        }) { completed in
            self.currentVC.removeFromParentViewController()
            destination.didMove(toParentViewController: self)
            self.currentVC = destination
            completion?()
        }
    }

    private func animateDismissTransition(to destination: UIViewController, completion: (() -> Void)? = nil) {
        currentVC.willMove(toParentViewController: nil)
        addChildViewController(destination)
        transition(from: currentVC, to: destination, duration: 0.3, options: [], animations: {
            destination.view.frame = self.view.bounds
        }) { completed in
            self.currentVC.removeFromParentViewController()
            destination.didMove(toParentViewController: self)
            self.currentVC = destination
            completion?()
        }
    }

}

extension AppDelegate {
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    var applicationStateController: ApplicationStateController {
        return window!.rootViewController as! ApplicationStateController
    }
}

//        StateController Transaction
//
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LOGIN_VC") as! LoginVC
//        let newRoot = UINavigationController.init(rootViewController: vc)
//        addChildViewController(newRoot)
//        newRoot.view.frame = view.bounds
//        view.addSubview(newRoot.view)
//        newRoot.didMove(toParentViewController: self)
//
//        currentVC.willMove(toParentViewController: nil)
//        currentVC.view.removeFromSuperview()
//        currentVC.removeFromParentViewController()
//
//        currentVC = newRoot
