//
//  AuthController.swift
//  Posta
//
//  Created by Dan Rudolf on 10/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation

class AuthController: NSObject {

    static var sharedInstance = AuthController()
    private let kPass = "password"
    private let kUser = "username"
    private let kJWT = "JWT"
    private let kTutorialCompleted = "tutorial_completed"

    let defaults = UserDefaults.standard
    let kReset = "Smcnm5HgrxLqm094xrCa4dqbEP6R5zLOuBq0FbIP"

    func getCurrentUserName() -> String?{
        guard let username = defaults.string(forKey: kUser) else{
            return nil
        }
        return username
    }

    func getCurrentPassword() -> String?{
        guard let password = defaults.string(forKey: kPass) else{
            return nil
        }
        return password
    }

    func logout(){
        self.defaults.set(nil, forKey: self.kPass)
        self.defaults.set(nil, forKey: self.kJWT)
    }

    func getCurrentToken() -> String? {
        guard let token = defaults.string(forKey: kJWT) else{
            return nil
        }
        return token
    }

    func shouldShowTutorial(version: Int) -> Bool {
        let showTutorial = defaults.integer(forKey: kTutorialCompleted)
        return version > showTutorial

        //show an agreement to a specific tutorial version starting at 1
        //if the tutorial viewed is less that the current tutorial version show the tutorial vc

    }



    func setNewUsername(username: String){
        self.defaults.set(username, forKey: self.kUser)
    }

    func setNewPassword(password: String){
        self.defaults.set(password, forKey: self.kPass)
    }

    func setNewToken(token: String){
        self.defaults.set(token, forKey: self.kJWT)
    }

    func setTutorialCompleted(completedVersion: Int){
        self.defaults.set(completedVersion, forKey: self.kTutorialCompleted)
    }

}
