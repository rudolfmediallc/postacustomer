//
//  StripeAPIClient.swift
//  Posta
//
//  Created by Dan Rudolf on 9/15/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class StripeAPIClient: NSObject {

    let env = Env.production

    func updatePaymentMethod(token: String, stripeId: String, completionHandler: @escaping (User?, Err?) -> Void) {
        let restService = RestService.init(environment: env)
        let body = ["default_payment": token, "stripe_id" : stripeId]
        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.POST)
        restService.setPath(pathString: restService.routes.post.newPaymentV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                print("serialize err")
                return
            }
            let userRes = serializerResponse as! UserRes
            completionHandler(userRes.user, nil)
            }.resume()
    }

    func getDefaultCardDetails(completionHandler: @escaping (Card?, Err?) -> Void){
        let restService = RestService.init(environment: env)
        let query = ["default_payment": UserController.currentUser.defaultPayment!, "stripe_id" : UserController.currentUser.stripeId!]
        restService.appendQueryParams(params: query)
        restService.setMethod(method: restService.methods.GET)
        restService.setPath(pathString: restService.routes.get.defaultCardV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: CardRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                print("serialize err")
                return
            }
            let cardRes = serializerResponse as! CardRes
            completionHandler(cardRes.card, nil)
            }.resume()
    }

}

