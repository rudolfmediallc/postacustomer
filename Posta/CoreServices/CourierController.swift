//
//  CourierController.swift
//  Posta
//
//  Created by Dan Rudolf on 9/12/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import CoreLocation

class CourierController: NSObject {

    let environment = Env.production

    static let sharedInstace = CourierController()

    var availableCouriers: [CourierLocation] = []

    func getAvaliableCouriers(location: CLLocationCoordinate2D, completionHandler: @escaping ([CourierLocation]?, Err?) -> Void) {
        let restService = RestService.init(environment: environment)
        let queryParams = ["lat": "\(location.latitude)", "lon" : "\(location.longitude)"]
        restService.setMethod(method: restService.methods.GET)
        restService.setPath(pathString: restService.routes.get.couriersV1)
        restService.appendQueryParams(params: queryParams)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: CourierLocationRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let locationRes = serializerResponse as! CourierLocationRes
            self.availableCouriers = locationRes.locations!
            completionHandler(locationRes.locations, nil)
            }.resume()
    }
}
