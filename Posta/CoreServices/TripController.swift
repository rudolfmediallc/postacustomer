//
//  TripController.swift
//  Posta
//
//  Created by Dan Rudolf on 7/6/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

protocol TripStateDelegate {
    func didCreateTrip()
    func didCancelTrip()
    func didUpdateTrip()
}

class TripController: NSObject {

    let environment = Env.production
    
    static let sharedInstace = TripController()
    var stateDelegate: TripStateDelegate?

    func submitTrip(trip: [AnyHashable: Any], completionHandler: @escaping (Trip?, Err?) -> Void) {
        let restService = RestService.init(environment: environment)
        restService.setBody(dictionary: trip)
        restService.setMethod(method: restService.methods.POST)
        restService.setPath(pathString: restService.routes.post.newTripV1)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: TripRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let tripRes = serializerResponse as! TripRes
            completionHandler(tripRes.trip, nil)
            DispatchQueue.main.async {
                self.stateDelegate?.didCreateTrip()
            }
            }.resume()
    }

    func getCurrentTrips(completionHandler: @escaping ([Trip]?, Err?) -> Void) {
        let restService = RestService.init(environment: environment)
        let queryParams = ["current": "true"]
        let pathParams = UserController.currentUser.profile.id
        restService.setMethod(method: restService.methods.GET)
        restService.setPath(pathString: restService.routes.get.tripsV1)
        restService.appendPathParams(params: pathParams)
        restService.appendQueryParams(params: queryParams)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: TripArrayRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let tripRes = serializerResponse as! TripArrayRes
            completionHandler(tripRes.trips, nil)
            }.resume()
    }

    func getCompletedTrips(completionHandler: @escaping ([Trip]?, Err?) -> Void) {
        let restService = RestService.init(environment: environment)
        let queryParams = ["current": "false"]
        let pathParams = UserController.currentUser.profile.id
        restService.setMethod(method: restService.methods.GET)
        restService.setPath(pathString: restService.routes.get.tripsV1)
        restService.appendPathParams(params: pathParams)
        restService.appendQueryParams(params: queryParams)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: TripArrayRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let tripRes = serializerResponse as! TripArrayRes
            completionHandler(tripRes.trips, nil)
            }.resume()
    }

    func updateTripStatus(trip_id: String, status: Int, confirmCode: String?, completionHandler: @escaping (Trip?, Err?) -> Void) {
        let restService = RestService.init(environment: environment)
        let pathParams = trip_id
        var body: [AnyHashable: Any] = ["status": status]
        if let code = confirmCode {
            body["confirm_code"] = code
        }
        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.PUT)
        restService.setPath(pathString: restService.routes.put.updateStatusV1)
        restService.appendPathParams(params: pathParams)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: TripRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let tripRes = serializerResponse as! TripRes
            completionHandler(tripRes.trip, nil)
            }.resume()
    }
}
