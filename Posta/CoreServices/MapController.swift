//
//  MapController.swift
//  Posta
//
//  Created by Dan Rudolf on 10/25/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Mapbox

enum Tracking {
    case enabled
    case disabled
    case anonymous
}

class MapController: NSObject, MGLMapViewDelegate {

    let parent: UIViewController
    let mapView: MGLMapView
    let userController = UserController.sharedInstace
    let courierController = CourierController.sharedInstace

    var isLocalObject: Bool = true
    var isTracking: Tracking
    var visibleTrip: AnyObject {
        didSet{
            if (visibleTrip is Trip) {
                isLocalObject = visibleTrip is Trip ? false : true
            }
        }
    }

    init(view: UIView, parent: UIViewController, tracking: Tracking, trip: AnyObject ) {
        self.parent = parent
        self.isTracking = tracking
        self.visibleTrip = trip
        mapView = MGLMapView(frame: view.bounds)
        super.init()

        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.styleURL = URL.init(string: "mapbox://styles/postateam/cjoocoszp3pot2sn9cbze6qpw")
        mapView.setCenter(userController.lastLocation!, animated: false)
        mapView.allowsTilting = false
        mapView.allowsRotating = false
        mapView.zoomLevel = 14
        mapView.tintColor = #colorLiteral(red: 0.1330988705, green: 0.2163389623, blue: 0.3238252997, alpha: 1)
        mapView.showsUserLocation = false
        mapView.delegate = self
        view.addSubview(mapView)
        updateTripData(trip: visibleTrip)
    }


    //MApView Delegate
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }


    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {

        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "MARKER")

        if (annotationView == nil){
            annotationView = getViewForAnnotation(annotation: annotation)
        }
        return annotationView
    }

    func getViewForAnnotation(annotation: MGLAnnotation) -> MGLAnnotationView{

        var annotationView = MGLAnnotationView.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 15))
        annotationView.isEnabled = true

        let imageView = UIImageView.init(frame: annotationView.frame)

        guard isLocalObject == false else{
            let trip = visibleTrip as! TripRequest
            switch (annotation.coordinate){

            case trip.destintationGeo?.getCoordinates():
                imageView.image = UIImage.init(named: "des_marker")
                break

            case UserController.currentUser.profile.location.getCoordinates():
                imageView.image = UIImage.init(named: "pickup_marker")
                break

            default:
                annotationView = MGLAnnotationView.init(frame: CGRect.init(x: 0, y: 0, width: 19, height: 36))
                annotationView.isEnabled = isTracking == .anonymous ? false : true

                let imageView = UIImageView.init(frame: annotationView.frame)
                imageView.image = UIImage.init(named: "courier_icon")
                annotationView.addSubview(imageView)
            }
            annotationView.addSubview(imageView)
            return annotationView
        }

        let trip = visibleTrip as! Trip
        switch (annotation.coordinate){

        case trip.pickupGeo.getCoordinates():
            imageView.image = UIImage.init(named: "pickup_marker")
            break
        case trip.destintationGeo.getCoordinates():
            imageView.image = UIImage.init(named: "des_marker")
            break

        default:
            let trackingAnnotationView = MGLAnnotationView.init(frame: CGRect.init(x: 0, y: 0, width: 19, height: 36))
            trackingAnnotationView.isEnabled = isTracking == .anonymous ? false : true

            let imageView = UIImageView.init(frame: trackingAnnotationView.frame)
            imageView.image = UIImage.init(named: "courier_icon")
            trackingAnnotationView.addSubview(imageView)

            return trackingAnnotationView
        }
        annotationView.addSubview(imageView)
        return annotationView
    }

    //Annotations
    func setAnnotations(for trip: TripRequest){

        let pickupAnnotation = MGLPointAnnotation.init()
        pickupAnnotation.coordinate = UserController.currentUser.profile.location.getCoordinates()
        pickupAnnotation.title = "Pickup"
        pickupAnnotation.subtitle = UserController.currentUser.profile.address.street

        mapView.addAnnotation(pickupAnnotation)
        mapView.selectAnnotation(pickupAnnotation, animated: true)
        mapView.setCenter(pickupAnnotation.coordinate, animated: false)
        mapView.setContentInset(UIEdgeInsets.init(top: 0, left: 0, bottom: (parent.view.frame.height/4), right: 0), animated: false)

        if (isTracking == .anonymous){
            addCouriersToMap(locations: courierController.availableCouriers)
        }

        guard let address = trip.destination?.address, let coordinates = trip.destintationGeo?.getCoordinates() else {
            return
        }

        let destinationAnnotation = MGLPointAnnotation.init()
        destinationAnnotation.coordinate = coordinates
        destinationAnnotation.title = "Dropoff"
        destinationAnnotation.subtitle = address.street

        mapView.addAnnotation(destinationAnnotation)

        mapView.selectAnnotation(destinationAnnotation, animated: true)
        mapView.setCenter(destinationAnnotation.coordinate, animated: false)
    }

    func setAnnotations(for trip: Trip){
        let pickupAnnotation = MGLPointAnnotation.init()
        pickupAnnotation.coordinate = trip.pickupGeo.getCoordinates()
        var title = "Pickup"
        if trip.status == 4{
            title = "Picked up"
        }
        pickupAnnotation.title = title
        pickupAnnotation.subtitle = trip.pickup.address.street

        mapView.addAnnotation(pickupAnnotation)

        let destinationAnnotation = MGLPointAnnotation.init()
        destinationAnnotation.coordinate = trip.destintationGeo.getCoordinates()
        var title2 = "Drop off"
        if trip.status == 4{
            title2 = "Delivered"
        }
        destinationAnnotation.title = title2
        destinationAnnotation.subtitle = trip.destination.address.street

        mapView.addAnnotation(destinationAnnotation)

        if trip.status == 2 {
            mapView.selectAnnotation(pickupAnnotation, animated: true)
        } else{
            mapView.selectAnnotation(destinationAnnotation, animated: true)
        }

        guard isTracking == .enabled, let courier = trip.courier else{
            return
        }

        let courierAnnotation = MGLPointAnnotation.init()
        courierAnnotation.coordinate = courier.location.getCoordinates()
        courierAnnotation.title = "Courier"
        courierAnnotation.subtitle = courier.name.first
        mapView.addAnnotation(courierAnnotation)
    }

    func addCouriersToMap(locations: [CourierLocation]?){
        DispatchQueue.main.async {
            self.addCouriersToMapThreadSafe(locations: locations)
        }
    }

    func addCouriersToMapThreadSafe(locations: [CourierLocation]?){
        removeCourierAnnotations()
        guard locations!.count != 0 else {
            return
        }
        for location in locations!{
            let courierMarker = MGLPointAnnotation()
            courierMarker.coordinate = (location.location!.getCoordinates())
            self.mapView.addAnnotation(courierMarker)
        }
    }

    func removeCourierAnnotations(){
        guard let annotations = mapView.annotations else {
            return print("Annotations Error")
        }
        for annotation in annotations{
            if annotation.coordinate != userController.lastLocation{
                mapView.removeAnnotation(annotation)
            }
        }
    }

    //View Rendering
    func updateTripData(trip: AnyObject){
        DispatchQueue.main.async {
            self.updateTripDataThreadSafe(trip: trip)
        }
    }
    func updateTripDataThreadSafe(trip: AnyObject){
        self.visibleTrip = trip
        if trip is TripRequest {
            isLocalObject = true
            if let annotations = mapView.annotations{
                mapView.removeAnnotations(annotations)
            }
            setAnnotations(for: trip as! TripRequest)
        }

        if trip is Trip{
            isLocalObject = false
            if let annotations = mapView.annotations{
                mapView.removeAnnotations(annotations)
            }
            setAnnotations(for: trip as! Trip)
        }
    }

    func setVisibleCoordinates(insets: UIEdgeInsets, coordinates: [CLLocationCoordinate2D]){
        let count = UInt.init(bitPattern: coordinates.count)
        mapView.setVisibleCoordinates(coordinates,
                                      count: count,
                                      edgePadding: insets,
                                      animated: true)

    }

}

