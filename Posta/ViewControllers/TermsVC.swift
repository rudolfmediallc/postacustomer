//
//  TermsVC.swift
//  Posta
//
//  Created by Dan Rudolf on 7/6/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class TermsVC: UIViewController {

    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var termsTextView: UITextView!

    let postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        let terms = Terms()
        termsTextView.text = terms.termsString
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
    }

    @IBAction func onMenuePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }

}
