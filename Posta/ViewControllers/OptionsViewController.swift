//
//  OptionsViewController.swift
//  Posta
//
//  Created by Dan Rudolf on 11/8/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

protocol OptionsControllerDelegate {
    func didSelectCallCourier()
    func didSelectCallRecipient()
    func didSelectCancelTrip()
}

class OptionsViewController: UIViewController {

    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var callCourierButton: UIButton!
    @IBOutlet weak var callRecipientButton: UIButton!
    @IBOutlet weak var cancelTripButton: UIButton!
    @IBOutlet weak var tripIdLabel: UILabel!

    var delegate: OptionsControllerDelegate?
    var currentTrip: Trip!


    override func viewDidLoad() {
        super.viewDidLoad()


        cancelButton.round(corners: .allCorners, radius: 5)
        callCourierButton.round(corners: .allCorners, radius: 5)
        callRecipientButton.round(corners: .allCorners, radius: 5)
        cancelTripButton.round(corners: .allCorners, radius: 5)

        roundViewCorners(view: alertView)
        setDropShadow(view: alertView)

        self.alphaView.alpha = 0
        self.alertView.alpha = 0
        self.alertView.center = CGPoint.init(x: self.view.center.x, y: self.view.center.y - 70)
    }

    override func viewDidAppear(_ animated: Bool) {
        animateIn()
    }


    @IBAction func didPressCallCourier(_ sender: Any) {
        delegate?.didSelectCallCourier()
        self.animateOut()
    }

    @IBAction func didPressCallRecipient(_ sender: Any) {
        delegate?.didSelectCallRecipient()
        self.animateOut()
    }

    @IBAction func didPressCancelTrip(_ sender: Any) {
        delegate?.didSelectCancelTrip()
        self.animateOut()
    }

    @IBAction func didPressCancel(_ sender: Any) {
        self.animateOut()
    }

    func animateIn(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.alphaView.alpha = 0.45
            self.alertView.alpha = 1
        }) { (finished) in

        }
    }

    func animateOut(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.alphaView.alpha = 0
            self.alertView.alpha = 0
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }

    func roundViewCorners(view: UIView){
        view.layer.cornerRadius = 8
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
    }

    func setDropShadow(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view.layer.shadowOpacity = 0.45
        view.layer.shadowRadius = 8
    }
}
