//
//  CurrentTripVC.swift
//  Posta
//
//  Created by Dan Rudolf on 7/12/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Mapbox

class PastTripVC: UIViewController, SnappyCardProtocol {

    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var closeButton: UIButton!

    let formatter = Formatter()

    var currentTrip: Trip!
    var mapController: MapController!
    let pickupAnnotation = MGLPointAnnotation.init()
    let destinationAnnotation = MGLPointAnnotation.init()

    var cardContents: PastTripDetailsView!
    var card: SnappyCard!
    var postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.closeButton.backgroundColor = UIColor.white

        self.mapController = MapController.init(view: mapContainer,
                                                parent: self,
                                                tracking: .disabled,
                                                trip: currentTrip as AnyObject)

        self.mapController.updateTripData(trip: currentTrip as AnyObject)

        postaTheme.applyButtonShadow(view: closeButton)
        self.closeButton.layer.cornerRadius = 6

    }

    override func viewDidLayoutSubviews() {
        if cardContents == nil {
            cardContents = PastTripDetailsView.init(frame: CGRect.init(origin: CGPoint.zero,
                                                                       size: CGSize.init(width: self.view.frame.width,
                                                                                         height: 520)))
            cardContents.populateDetailsForTripState(trip: currentTrip)
        }
        if card == nil {
            card = SnappyCard.init(parent: self, content: cardContents, verticalBound: 240)
            card.delegate = self
            self.view.addSubview(card)
            card.setCollapsed()
        }
    }

    func didCollapseCard() {
        let coords = [currentTrip.pickupGeo.getCoordinates(), currentTrip.destintationGeo.getCoordinates()]
        let inset = UIEdgeInsets.init(top: 140, left: 95, bottom: 340, right: 80)
        mapController.setVisibleCoordinates(insets: inset, coordinates: coords)
    }

    func didExpantCard() {

    }

    //MARK: Actions and Selectors
    @IBAction func onBackPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

//    //MARK: View Rendering and apperance
//    func addShadowAndCornerRadius(view: UIView){
//        var shadowLayer: CAShapeLayer!
//        shadowLayer = CAShapeLayer()
//        shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 0).cgPath
//        shadowLayer.fillColor = UIColor.white.cgColor
//
//        shadowLayer.shadowColor = UIColor.darkGray.cgColor
//        shadowLayer.shadowPath = shadowLayer.path
//        shadowLayer.shadowOffset = CGSize(width: -4.0, height: 4.0)
//        shadowLayer.shadowOpacity = 0.65
//        shadowLayer.shadowRadius = 10
//
//        view.layer.insertSublayer(shadowLayer, at: 0)
//    }
//
//    func circleView(view: UIView){
//        view.layer.masksToBounds = false
//        view.layer.cornerRadius = (view.frame.height/2)
//    }
//
//    func setDropShadow(view: UIView){
//        view.layer.shadowColor = UIColor.black.cgColor
//        view.layer.shadowOffset = CGSize(width: 0.0, height: -1.0)
//        view.layer.shadowOpacity = 0.45
//        view.layer.shadowRadius = 2
//    }
//
//    func setButtonShadow(view: UIView){
//        view.layer.shadowColor = UIColor.black.cgColor
//        view.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
//        view.layer.shadowOpacity = 0.45
//        view.layer.shadowRadius = 2
//    }

}
