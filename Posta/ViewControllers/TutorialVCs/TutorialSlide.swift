//
//  TutorialSlide.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 8/21/19.
//  Copyright © 2019 com.rudolfmedia. All rights reserved.
//

import UIKit

class TutorialSlide: UIView {

    @IBOutlet weak var footerLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var imageBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var imageTopSpace: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var footerLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var secondaryTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var secondaryTitleLabel: UILabel!

    func setConstrainsForScreenSize(height: CGFloat, slide: Int){
        height <= 667.0 ? setConstraintsForSmallerScreen(slide: slide) : setConstraintsForLargerScreen(slide: slide)
    }

    func setConstraintsForSmallerScreen(slide: Int){
        switch slide {
        case 1:
            titleSpaceConstraint.constant = 24.0
            secondaryTitleHeight.constant = 0.0
            secondaryTitleLabel.isHidden = true
            imageTopSpace.constant = 24.0
            imageViewHeight.constant = 434.0
            imageBottomSpace.constant = 12.0
            footerLabelHeight.constant = 55.0
            break
        case 2:
            titleSpaceConstraint.constant = 24.0
            secondaryTitleHeight.constant = 0.0
            secondaryTitleLabel.isHidden = true
            imageTopSpace.constant = 16.0
            imageViewHeight.constant = 434.0
            imageBottomSpace.constant = 16.0
            footerLabelHeight.constant = 30.0
            break
        case 3:
            titleSpaceConstraint.constant = 24.0
            secondaryTitleHeight.constant = 0.0
            secondaryTitleLabel.isHidden = true
            imageTopSpace.constant = 16.0
            imageViewHeight.constant = 434.0
            imageBottomSpace.constant = 16.0
            footerLabelHeight.constant = 50.0
            break
        case 4:
            titleSpaceConstraint.constant = 12.0
            secondaryTitleHeight.constant = 50.0
            secondaryTitleLabel.isHidden = false
            imageTopSpace.constant = 16.0
            imageViewHeight.constant = 390.0
            imageBottomSpace.constant = 16.0
            footerLabelHeight.constant = 50.0
            break
        default:
            break
        }

    }

    func setConstraintsForLargerScreen(slide: Int){
        switch slide {
        case 1:
            titleSpaceConstraint.constant = 30.0
            secondaryTitleHeight.constant = 0.0
            secondaryTitleLabel.isHidden = true
            imageTopSpace.constant = 24.0
            imageViewHeight.constant = 500.0
            imageBottomSpace.constant = 24.0
            footerLabelHeight.constant = 55.0
            break
        case 2:
            titleSpaceConstraint.constant = 30.0
            secondaryTitleHeight.constant = 0.0
            secondaryTitleLabel.isHidden = true
            imageTopSpace.constant = 24.0
            imageViewHeight.constant = 500.0
            imageBottomSpace.constant = 24.0
            footerLabelHeight.constant = 30.0
            break
        case 3:
            titleSpaceConstraint.constant = 30.0
            secondaryTitleHeight.constant = 0.0
            secondaryTitleLabel.isHidden = true
            imageTopSpace.constant = 24.0
            imageViewHeight.constant = 500.0
            imageBottomSpace.constant = 24.0
            footerLabelHeight.constant = 50.0
            break
        case 4:
            titleSpaceConstraint.constant = 24.0
            secondaryTitleHeight.constant = 50.0
            secondaryTitleLabel.isHidden = false
            imageTopSpace.constant = 24.0
            imageViewHeight.constant = 464.0
            imageBottomSpace.constant = 12.0
            footerLabelHeight.constant = 50.0
            break
        default:
            break
        }
    }
}
