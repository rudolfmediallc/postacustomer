//
//  TutorialVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 8/21/19.
//  Copyright © 2019 com.rudolfmedia. All rights reserved.
//

import UIKit

class TutorialVC: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var continueButton: UIButton!

    var slides: [TutorialSlide] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
    }

    override func viewDidLayoutSubviews() {
        slides = createSlides()

        setupSlideScrollView(slides: slides)

        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubview(toFront: pageControl)
    }


    func setupSlideScrollView(slides : [TutorialSlide]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true

        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }

    func createSlides() -> [TutorialSlide] {

        let height = self.view.frame.height

        let slide1:TutorialSlide = Bundle.main.loadNibNamed("TutorialSlide", owner: self, options: nil)?.first as! TutorialSlide
        slide1.setConstrainsForScreenSize(height: height, slide: 1)
        slide1.imageView.image = UIImage(named: "tutorial_one")
        slide1.titleLabel.text = "Set a Pickup Location"
        slide1.footerLabel.text = "Set a pickup location for the courier to receive your package"

        let slide2:TutorialSlide = Bundle.main.loadNibNamed("TutorialSlide", owner: self, options: nil)?.first as! TutorialSlide
        slide2.setConstrainsForScreenSize(height: height, slide: 2)
        slide2.imageView.image = UIImage(named: "tutorial_two")
        slide2.titleLabel.text = "Set a Drop Off Location"
        slide2.footerLabel.text = "Set your package's destination address"

        let slide3:TutorialSlide = Bundle.main.loadNibNamed("TutorialSlide", owner: self, options: nil)?.first as! TutorialSlide
        slide3.setConstrainsForScreenSize(height: height, slide: 3)
        slide3.imageView.image = UIImage(named: "tutorial_three")
        slide3.titleLabel.text = "Describe Your Package"
        slide3.footerLabel.text = "Enter some information about the package and the recipient"

        let slide4:TutorialSlide = Bundle.main.loadNibNamed("TutorialSlide", owner: self, options: nil)?.first as! TutorialSlide
        slide4.setConstrainsForScreenSize(height: height, slide: 4)
        slide4.imageView.image = UIImage(named: "tutorial_four")
        slide4.titleLabel.text = "Your Courier is Scheduled"
        slide4.footerLabel.text = "Once they respond we’ll send out your courier!"
        slide4.secondaryTitleLabel.text = "Posta will contact the recipient via text message to confirm the destination "

        return [slide1, slide2, slide3, slide4]
        
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentOffset.y = 0
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        if pageIndex == 3 {
            continueButton.isHidden = false
            pageControl.isHidden = true
        } else{
            continueButton.isHidden = true
            pageControl.isHidden = false
        }
    }

    @IBAction func onContinuePressed(_ sender: Any) {
        Tutorial().setTutorialCompleted()
        self.dismiss(animated: true, completion: nil)
    }

}
