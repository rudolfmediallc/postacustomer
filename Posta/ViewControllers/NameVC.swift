//
//  NameVC.swift
//  Posta
//
//  Created by Dan Rudolf on 6/22/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import ContactsUI

class NameVC: UIViewController, CNContactPickerDelegate {


    @IBOutlet weak var contactsButton: UIButton!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var doneButtonView: UIView!
    @IBOutlet weak var contentsField: UITextField!
    
    var tripBuiler = TripBuilder.sharedInstance


    override func viewDidLoad() {
        super.viewDidLoad()
        let doneTap = UITapGestureRecognizer.init(target: self, action: #selector(onDonePressed))
        self.doneButtonView.addGestureRecognizer(doneTap)

        self.firstNameField.becomeFirstResponder()

        self.containerView.alpha = 0
        self.alphaView.alpha = 0
    }

    override func viewDidAppear(_ animated: Bool) {
        self.animateIn()
    }

    override func viewDidLayoutSubviews() {
        doneButtonView.round(corners: .allCorners, radius: 5)
        contactsButton.round(corners: .allCorners, radius: 5)
        containerView.round(corners: .allCorners, radius: 8)
    }

    //Text Formatting
    func stripAndCapitalize(str: String) -> String{
        let trimed = str.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimed.capitalized
    }

    func formatPhone(phone: String?) -> String{
        let trimed = phone!.trimmingCharacters(in: .whitespacesAndNewlines)
        guard trimed.count > 10 else {
            return trimed
        }
        let count = (trimed.count - 10)
        let subStr = trimed.dropFirst(count)
        return String(subStr)
    }

    @IBAction func onGetContactsPressed(_ sender: Any) {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
                , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
    }

    @objc func onDonePressed(){

        resignResponderChain()

        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .info)
        guard !self.firstNameField.text!.isEmpty else{
            banner.show(message: "Please enter the recipient's name.")
            return
        }

        guard !self.lastNameField.text!.isEmpty else{
            banner.show(message: "Please enter the recipient's name.")
            return
        }

        guard !self.phoneNumberField.text!.isEmpty else{
            banner.show(message: "We'll need to recipient's phone number to confirm their receiving address.")
            return
        }

        guard !self.contentsField.text!.isEmpty else{
            banner.show(message: "Please add a breif description of the contents.")
            return
        }

        let name = Name.init(first: stripAndCapitalize(str: firstNameField.text ?? ""),
                             last: stripAndCapitalize(str: lastNameField.text ?? ""))

        tripBuiler.setReceipientDetails(name: name,
                                        phone: formatPhone(phone: phoneNumberField.text),
                                        description: self.contentsField.text)

        self.animateOut()
    }
    
    func resignResponderChain(){
        if self.firstNameField.canResignFirstResponder{
            self.firstNameField.resignFirstResponder()
        }
        if self.lastNameField.canResignFirstResponder{
            self.lastNameField.resignFirstResponder()
        }
        if self.phoneNumberField.canResignFirstResponder{
            self.phoneNumberField.resignFirstResponder()
        }
        if self.contentsField.canResignFirstResponder{
            self.contentsField.resignFirstResponder()
        }
    }

    //Contact Picker Delegate

    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {

    }

    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // user name
        let userPhoneNumbers: [CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        let firstPhoneNumber: CNPhoneNumber = userPhoneNumbers[0].value
        self.phoneNumberField.text = formatPhone(phone: firstPhoneNumber.stringValue)

        self.firstNameField.text = contact.givenName
        self.lastNameField.text = contact.familyName

        self.contentsField.becomeFirstResponder()
    }

    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {

    }
    //View Rendering and Presentation
    func animateIn(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.alphaView.alpha = 0.45
            self.containerView.alpha = 1
        }) { (finished) in

        }
    }

    func animateOut(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.alphaView.alpha = 0
            self.containerView.alpha = 0
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }

}
