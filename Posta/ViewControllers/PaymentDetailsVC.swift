//
//  PaymentDetailsVC.swift
//  Posta
//
//  Created by Dan Rudolf on 9/14/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Stripe

class PaymentDetailsVC: UIViewController, STPPaymentContextDelegate {
    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {

    }

    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {

    }

    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPErrorBlock) {

    }

    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {

    }



}
