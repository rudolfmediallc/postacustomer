//
//  HomeVC.swift
//  Posta
//
//  Created by Dan Rudolf on 6/10/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Mapbox
import MapboxGeocoder
import CoreLocation

class HomeVC: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, TripBuilderDelegate {

    @IBOutlet weak var titleBar: UIView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var addressLineOneLabel: UILabel!
    @IBOutlet weak var addressLineTwoLabel: UILabel!
    @IBOutlet weak var newTripSubview: UIView!
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var changeLocationButton: UIButton!

    let userController = UserController.sharedInstace
    let courierController = CourierController.sharedInstace
    let tripBuilder = TripBuilder.sharedInstance
    let geoController = GeoController.sharedInstace
    let stripeController = StripeAPIClient()

    let restService = RestService.init(environment: .production)
    let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
    let postaTheme = PostaTheme()

    var couriersAvailable: Bool = false
    var mapController: MapController!
    var pollingTimer: Timer!

    override func viewDidLoad() {
        super.viewDidLoad()

        tripBuilder.builderDelegate = self
        loadingView.presenting = self
        loadingView.modalPresentationStyle = .overCurrentContext

        self.mapController = MapController.init(view: mapContainer, parent: self, tracking: .anonymous, trip: tripBuilder.currentTrip as AnyObject)
        self.mapController.updateTripData(trip: tripBuilder.currentTrip as AnyObject)
        self.mapController.mapView.setZoomLevel(14, animated: true)

        view.bringSubview(toFront: newTripSubview)
        roundViewCorners(view: newTripSubview)

        changeLocationButton.round(corners: .allCorners, radius: 5)
        couriersAvailable = courierController.availableCouriers.count != 0

        updateDisplayAddress(address: UserController.currentUser.profile.address)

    }

    override func viewDidLayoutSubviews() {
        postaTheme.applyDownwardDropShadow(view: titleBar)
        postaTheme.applyUpwardDropShadow(view: newTripSubview)
    }

    override func viewDidAppear(_ animated: Bool) {
        self.mapController.mapView.setZoomLevel(14, animated: true)
        self.beginPollingForAvailableCouriers()
        checkTutorialAndUserTermsAgreement()
    }

    override func viewWillDisappear(_ animated: Bool) {
        guard pollingTimer != nil else {
            return
        }
        self.endPollingForAvailableCouriers()
    }

    func checkTutorialAndUserTermsAgreement(){

        guard Tutorial().shoulShowTutorial() == false else{
            showTutorial()
            return
        }

        guard let _ = UserController.currentUser.terms.agreedOn, let version =  UserController.currentUser.terms.version else{
            showTermsAgreement()
            return
        }

        guard version >= Terms().version else{
            showTermsAgreement()
            return
        }
    }

    func showTermsAgreement(){
        let t = NewTermsVC.init(nibName: "NewTermsVC", bundle: Bundle.main)
        present(t, animated: true, completion: nil)
    }


    func showTutorial(){
        let t = TutorialVC.init(nibName: "TutorialVC", bundle: Bundle.main)
        present(t, animated: true, completion: nil)
    }

    // Trip Builder Delegate
    func didSetDropoff(tripRequest: TripRequest) {}
    func didSetDetails(tripRequest: TripRequest) {}
    func didSetInstructions(tripRequest: TripRequest) {}

    func didSetPickup(address: Address, coordinates: CLLocationCoordinate2D) {
        self.addressLineOneLabel.text = address.getStreet(type: .full)
        self.addressLineTwoLabel.text = address.getLocaleAddress(type: .short)

        self.mapController.updateTripData(trip: tripBuilder.currentTrip as AnyObject)
    }

    func updateDisplayAddress(address: Address){
        self.addressLineOneLabel.text = address.getStreet(type: .full)
        self.addressLineTwoLabel.text = address.getLocaleAddress(type: .short)
    }

    func beginPollingForAvailableCouriers(){
        self.pollingTimer = Timer.scheduledTimer(withTimeInterval: 15.0, repeats: true) { timer in
            self.refreshAvailableCouriers()
        }
    }

    func endPollingForAvailableCouriers(){
        self.pollingTimer.invalidate()
    }

    func refreshAvailableCouriers(){
        courierController.getAvaliableCouriers(location: userController.lastLocation!) { (locations, err) in
            guard err == nil else{
                self.couriersAvailable = false
                return
            }
            guard !locations!.isEmpty else{
                self.couriersAvailable = false
                return
            }
            print("\(self.courierController.availableCouriers.count) Couriers Available")
            self.couriersAvailable = true
            self.mapController.addCouriersToMap(locations: locations!)
        }
    }

    func presentTripVC(with card: Card){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            let newTripVC = NewTripVC.init(nibName: "NewTripVC", bundle: Bundle.main)
            newTripVC.defaultCard = card
            self.present(newTripVC, animated: true, completion: nil)
        })
    }

    @IBAction func onChangeLocationPressed(_ sender: Any) {
        let destinationVC = AddressSearchVC.init(nibName: "AddressSearchVC", bundle: Bundle.main)
        destinationVC.isPickupLocation = true
        self.present(destinationVC, animated: true, completion: nil)
    }

    @IBAction func onNextPressed(_ sender: Any) {

        guard getNewTripPermission() else {
            return
        }

        guard let senderJSON = restService.coder.encodeJSON(codable: UserController.currentUser.profile) else {
            return
        }

        guard checkBounds(coord: UserController.currentUser.profile.location.getCoordinates()) else {
            return
        }

        loadingView.show()
        self.updateCurrentAddress(json: senderJSON )
    }

    func checkBounds(coord: CLLocationCoordinate2D) -> Bool {
        let bounds = Bounds.init()
            guard bounds.checkIsInRegion(point: coord) == true else{
                let outOfRegionAlert = OutOfRegionAlert.init(nibName: "OutOfRegionAlert", bundle: Bundle.main)
                outOfRegionAlert.modalPresentationStyle = .overCurrentContext
                self.present(outOfRegionAlert, animated: false, completion: nil)
            return false
        }

        return true
    }


    func getNewTripPermission() -> Bool{
        guard checkCouriersAvailable() else {
            return false
        }
        guard checkDefaultPayment() else{
            return false
        }
        return true
    }

    func checkCouriersAvailable() -> Bool{
        guard couriersAvailable == true else{
            let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.setAlertImage(alertType: .courier)
            alertVC.setAlertTitle(title: "All Couriers Offline")
            alertVC.setAlertMessage(body: "There are currently no couriers online in your area. Please check back in a little while.")
            self.present(alertVC, animated: false, completion: nil)
            return false
        }
        return true
    }

    func checkDefaultPayment() -> Bool{
        guard UserController.currentUser.defaultPayment != nil else {
            let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.setAlertImage(alertType: .payment)
            alertVC.setAlertTitle(title: "Payment Details")
            alertVC.setAlertMessage(body: "Please setup your payment info in the 'Payments' tap before requesting a courier.")
            self.present(alertVC, animated: false, completion: nil)
            return false
        }
        return true
    }

    func updateCurrentAddress(json: [AnyHashable : Any]){
        userController.updateCurrentLocation(sender: json) { (sender, err) in
            guard err == nil else{
                self.loadingView.dismissDelayed()
                return
            }
            self.getCardDetails()
        }
    }

    func getCardDetails(){
        stripeController.getDefaultCardDetails { (card, err) in
            self.loadingView.dismissDelayed()
            guard err == nil else{
                DispatchQueue.main.async {
                    let banner = BannerView.init(parent: self)
                    banner.setDurationAndLevel(duration: .short, level: .warn)
                    banner.showDelayed(message: "There was an error retreiving your payment details, please try again")
                }
                return
            }
            self.presentTripVC(with: card!)
        }
    }

    @IBAction func onMenuPress(_ sender: Any) {
        guard let main = self.parent as? MainVC else{
            return
        }
        main.shouledShowMenu(show: true)
    }

    func roundViewCorners(view: UIView){
        view.layer.cornerRadius = 12
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

}
