//
//  SignUpVC.swift
//  Posta
//
//  Created by Dan Rudolf on 8/24/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!

    let userController = UserController.sharedInstace
    let authController = AuthController.sharedInstance
    let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)


    override func viewDidLoad() {
        super.viewDidLoad()
        roundViews(view: self.signupButton)
        let t = UITapGestureRecognizer.init(target: self, action: #selector(resignResponders))
        self.view.addGestureRecognizer(t)
        loadingView.modalPresentationStyle = .overCurrentContext
    }

    func roundViews(view: UIView){
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 4
    }

    @objc func resignResponders(){
        firstNameField.resignFirstResponder()
        lastNameField.resignFirstResponder()
        phoneField.resignFirstResponder()
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        confirmPasswordField.resignFirstResponder()
    }

    func validateFields() -> NewUser?{
        if (firstNameField.text?.isEmpty)!{
            let banner = BannerView.init(parent: self)
            banner.setDurationAndLevel(duration: .short, level: .info)
            banner.show(message: "Please enter a first name.")
            return nil
        }
        if (lastNameField.text?.isEmpty)!{
            let banner = BannerView.init(parent: self)
            banner.setDurationAndLevel(duration: .short, level: .info)
            banner.show(message: "Please enter a last name.")
            return nil
        }
        if (phoneField.text?.isEmpty)!{
            let banner = BannerView.init(parent: self)
            banner.setDurationAndLevel(duration: .short, level: .info)
            banner.show(message: "Please enter your phone number.")
            return nil
        }
        if (emailField.text?.isEmpty)!{
            let banner = BannerView.init(parent: self)
            banner.setDurationAndLevel(duration: .short, level: .info)
            banner.show(message: "Please enter an email address.")
            return nil
        }
        if (passwordField.text?.isEmpty)!{
            let banner = BannerView.init(parent: self)
            banner.setDurationAndLevel(duration: .short, level: .info)
            banner.show(message: "Please create a password.")
            return nil
        }
        if (confirmPasswordField.text?.isEmpty)!{
            let banner = BannerView.init(parent: self)
            banner.setDurationAndLevel(duration: .short, level: .info)
            banner.show(message: "Please confrim your new password.")
            return nil
        }
        let phone = phoneField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if phone?.count != 10 {
            let banner = BannerView.init(parent: self)
            banner.setDurationAndLevel(duration: .short, level: .warn)
            banner.show(message: "Please enter a valid phone number.")
            return nil
        }
        if passwordField.text != confirmPasswordField.text{
            let banner = BannerView.init(parent: self)
            banner.setDurationAndLevel(duration: .short, level: .warn)
            banner.show(message: "The confirmation password does not match")
            return nil
        }

        let user = NewUser.init(firstName: format(str: firstNameField.text),
                                lastName: format(str: lastNameField.text),
                                email: format(str: emailField.text?.lowercased()),
                                phone: format(str: phoneField.text),
                                pass: passwordField.text!)

        return user
    }

    func format(str: String?) -> String{
        let formatted = str?.trimmingCharacters(in: .whitespacesAndNewlines)
        return formatted!
    }

    func presentApplicationRoot(){
        DispatchQueue.main.async {
            let vc = self.presentingViewController as! ApplicationStateController
            vc.dismiss(animated: true, completion: nil)
            vc.presentLogin()
        }
    }

    @IBAction func onSignupPressed(_ sender: Any) {
        guard let newUser = validateFields() else{
            return
        }
        self.present(loadingView, animated: false, completion: nil)
        userController.createUser(user: newUser) { (user, err) in
            self.loadingView.dismissImediate()
            guard err == nil else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
                    alertVC.modalPresentationStyle = .overCurrentContext
                    alertVC.setAlertImage(alertType: .fail)
                    alertVC.setAlertTitle(title: "Could Not Create Account")
                    alertVC.setAlertMessage(body: err!.message)
                    self.present(alertVC, animated: false, completion: nil)
                }
                return
            }
            UserController.currentUser = user!
            self.authController.setNewUsername(username: newUser.email)
            self.authController.setNewPassword(password: newUser.password)
            self.presentApplicationRoot()
        }
    }

    @IBAction func onCancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
