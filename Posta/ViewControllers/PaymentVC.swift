//
//  PaymentVC.swift
//  Posta
//
//  Created by Dan Rudolf on 6/10/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Stripe

class PaymentVC: UIViewController, STPAddCardViewControllerDelegate {

    @IBOutlet weak var defaultTitle: UILabel!
    @IBOutlet weak var addCardButton: UIButton!
    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardIcon: UIImageView!
    @IBOutlet weak var last4Label: UILabel!
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var detailsContainer: UIView!
    @IBOutlet weak var billingZipLabel: UILabel!
    @IBOutlet weak var expDateLabel: UILabel!
    
    let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)

    var stripeController = StripeAPIClient()
    var postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.modalPresentationStyle = .overCurrentContext

        detailsContainer.isHidden = true

        if (UserController.currentUser.defaultPayment != nil){
            requestCardDetails()
        }
    }

    override func viewDidLayoutSubviews() {
        addCardButton.round(corners: .allCorners, radius: 5)
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
        renderCardView()
    }

    //MARK: Stripe Payment Controller Delegate
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        dismiss(animated: true)
    }

//    func addCardViewController(_ addCardViewController: STPAddCardViewController,
//                               didCreatePaymentMethod paymentMethod: STPPaymentMethod,
//                               completion: @escaping STPErrorBlock) {
//                stripeController.updatePaymentMethod(token: paymentMethod.stripeId,
//                                                     stripeId: UserController.currentUser.stripeId!) { (user, err) in
//                    guard err == nil else {
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//                            let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
//                            alertVC.modalPresentationStyle = .overCurrentContext
//                            alertVC.setAlertImage(alertType: .fail)
//                            alertVC.setAlertTitle(title: "Card Validation Failed")
//                            alertVC.setAlertMessage(body: err!.message)
//                            self.present(alertVC, animated: false, completion: nil)
//                        }
//                        return
//                    }
//                    UserController.currentUser.defaultPayment = user?.defaultPayment
//                    self.requestCardDetails()
//                }
//                dismiss(animated: true)
//
//    }

    func addCardViewController(_ addCardViewController: STPAddCardViewController,
                               didCreateToken token: STPToken,
                               completion: @escaping STPErrorBlock) {
        stripeController.updatePaymentMethod(token: token.tokenId,
                                             stripeId: UserController.currentUser.stripeId!) { (user, err) in
            guard err == nil else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
                    alertVC.modalPresentationStyle = .overCurrentContext
                    alertVC.setAlertImage(alertType: .fail)
                    alertVC.setAlertTitle(title: "Card Validation Failed")
                    alertVC.setAlertMessage(body: err!.message)
                    self.present(alertVC, animated: false, completion: nil)
                }
                return
            }
            UserController.currentUser.defaultPayment = user?.defaultPayment
            self.requestCardDetails()
        }
        dismiss(animated: true)
    }

    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateSource source: STPSource, completion: @escaping STPErrorBlock) {
        
    }

    @IBAction func presentNewCardControler(_ sender: UIButton){
        let theme = STPTheme()
        theme.accentColor = #colorLiteral(red: 0.1450980392, green: 0.5411764706, blue: 0.8705882353, alpha: 1)
        theme.font = UIFont.init(name: "Avenir-Book", size: 17)
        theme.emphasisFont = UIFont.init(name: "Avenir-Medium", size: 17)


        let config = STPPaymentConfiguration.shared()
        config.requiredBillingAddressFields = .zip
        let addCardViewController = STPAddCardViewController.init(configuration: config, theme: theme)
        addCardViewController.delegate = self
        let navigationController = UINavigationController.init(rootViewController: addCardViewController)
        navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Avenir-Medium", size: 17)!]
        present(navigationController, animated: true, completion: nil)
    }

    @IBAction func onMenuePressed(_ sender: Any) {
        guard let main = self.parent as? MainVC else{
            return
        }
        main.shouledShowMenu(show: true)
    }

    func addShadow(view: UIView){
        var shadowLayer: CAShapeLayer!
        shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 2).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor

        shadowLayer.shadowColor = UIColor.darkGray.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = 2

        view.layer.insertSublayer(shadowLayer, at: 0)
    }

    func renderCardView(){
        postaTheme.applyDownwardDropShadow(view: cardView)
        cardView.layer.cornerRadius = 8

        cardIcon.layer.cornerRadius = 4
        cardIcon.layer.masksToBounds = true
    }

    func requestCardDetails(){
        self.present(loadingView, animated: false, completion: nil)
        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)
        stripeController.getDefaultCardDetails { (card, err) in
            self.loadingView.dismissDelayed()
            guard err == nil else{
                banner.showDelayed(message: "Something went wrong retreiving your payment details. Please try again")
                return
            }
            guard card != nil else{
                banner.showDelayed(message: "Something went wrong retreiving your payment details. Please try again")
                return
            }
            DispatchQueue.main.async {
                self.renderCardDetails(card: card!)
            }
        }
    }

    func renderCardDetails(card: Card){
        detailsContainer.isHidden = false
        cardView.backgroundColor = postaTheme.darkFill
        cardView.alpha = 1
        last4Label.text = "\u{2022}\u{2022}\u{2022}\u{2022}\u{2022}\u{2022}\(card.lastFour)"
        last4Label.textColor = UIColor.white
        defaultTitle.textColor = UIColor.white
        cardIcon.image = card.getCardImage()
        expDateLabel.text = "\(card.expMonth)/\(card.extYear)"
        billingZipLabel.text = card.addressZip
    }

}
