//
//  CurrentTripVC.swift
//  Posta
//
//  Created by Dan Rudolf on 7/12/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Mapbox

class CurrentTripVC: UIViewController, MGLMapViewDelegate, SnappyCardProtocol {

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var mapContainer: UIView!

    var currentTrip: Trip!
    let pickupAnnotation = MGLPointAnnotation.init()
    let destinationAnnotation = MGLPointAnnotation.init()
    var mapController: MapController!
    var postaTheme = PostaTheme()

    var cardContents: CurrentDeliveryDetailsView!
    var card: SnappyCard!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mapController = MapController.init(view: mapContainer, parent: self, tracking: .enabled, trip: currentTrip as AnyObject)
        self.mapController.updateTripData(trip: currentTrip as AnyObject)
        
        postaTheme.applyButtonShadow(view: closeButton)
        self.closeButton.layer.cornerRadius = 6
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if cardContents == nil {
            cardContents = CurrentDeliveryDetailsView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.frame.width, height: 412)))
            cardContents.parentVC = self
            cardContents.populateTripDetailsForState(trip: currentTrip)
        }

        if card == nil {
            card = SnappyCard.init(parent: self, content: cardContents, verticalBound: 240)
            self.view.addSubview(card)
            card.delegate = self
            card.setCollapsed()
        }

    }

    func didCollapseCard() {
        let inset = UIEdgeInsets.init(top: 140, left: 95, bottom: 340, right: 80)
        var visible = [currentTrip.pickupGeo.getCoordinates(),
                       currentTrip.destintationGeo.getCoordinates()]

        if currentTrip.courier?.location.coordinates != nil{
            let courierLocation = currentTrip.courier?.location.getCoordinates()
            visible.append(courierLocation!)
            mapController.setVisibleCoordinates(insets: inset, coordinates: visible)
        }
    }

    func didExpantCard() { }

    //MARK: Actions and Selectors
    @IBAction func onBackPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
