//
//  ServiceAreaVC.swift
//  Posta
//
//  Created by Dan Rudolf on 11/14/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Mapbox

class ServiceAreaVC: UIViewController, MGLMapViewDelegate {

    @IBOutlet weak var titleBar: UIView!
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var messageBar: UIView!

    var mapView: MGLMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addShadow(view: titleBar)
        addShadowVertical(view: messageBar)
        setupMapView()
    }

    func setupMapView(){
        mapView = MGLMapView(frame: mapContainer.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.styleURL = URL.init(string: "mapbox://styles/postateam/cjoocoszp3pot2sn9cbze6qpw")
        mapView.allowsTilting = false
        mapView.allowsRotating = false
        mapView.zoomLevel = 12
        mapView.tintColor = #colorLiteral(red: 0.1330988705, green: 0.2163389623, blue: 0.3238252997, alpha: 1)
        mapView.showsUserLocation = false
        mapView.delegate = self
        mapContainer.addSubview(mapView)
        let g = UITapGestureRecognizer.init(target: self, action: #selector(printDeets))
        self.view.addGestureRecognizer(g)
    }

    @objc func printDeets(){
        print(mapView.centerCoordinate)
        print(mapView.zoomLevel)
    }

    override func viewDidAppear(_ animated: Bool) {
        drawShape()
    }

    func drawShape() {
        // Create a coordinates array to hold all of the coordinates for our shape.
        let bounds = Bounds.init()
        var coordinates = bounds.getDelieryZoneCoordinates()

        let shape = MGLPolygon(coordinates: &coordinates, count: UInt(coordinates.count))
        mapView.addAnnotation(shape)
        mapView.setVisibleCoordinates(coordinates, count: UInt(coordinates.count), edgePadding: UIEdgeInsets(top: 150, left: 30, bottom: 150, right: 20), animated: true)
    }

    func mapView(_ mapView: MGLMapView, alphaForShapeAnnotation annotation: MGLShape) -> CGFloat {
        return 0.40
    }
    func mapView(_ mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        return .white
    }

    func mapView(_ mapView: MGLMapView, fillColorForPolygonAnnotation annotation: MGLPolygon) -> UIColor {
        return UIColor(red: 49/255, green: 191/255, blue: 174/255, alpha: 1)
    }

    @IBAction func onDonePressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func addShadow(view: UIView){
        var shadowLayer: CAShapeLayer!
        shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 2).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor

        shadowLayer.shadowColor = UIColor.darkGray.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = 2

        view.layer.insertSublayer(shadowLayer, at: 0)
    }

    func addShadowVertical(view: UIView){
        var shadowLayer: CAShapeLayer!
        shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 2).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor

        shadowLayer.shadowColor = UIColor.darkGray.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: -2.0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = 2

        view.layer.insertSublayer(shadowLayer, at: 0)
    }

}
