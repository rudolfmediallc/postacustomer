//
//  InstructionsVC.swift
//  Posta
//
//  Created by Dan Rudolf on 6/22/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class InstructionsVC: UIViewController {

    @IBOutlet weak var instructionsField: UITextField!
    @IBOutlet weak var doneButtonView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var alphaView: UIView!

    let tripBuilder = TripBuilder.sharedInstance
    let notificationCenter = NotificationCenter.default

    override func viewDidLoad() {
        super.viewDidLoad()
        addShadowAndCornerRadius(view: containerView)

        let doneTap = UITapGestureRecognizer.init(target: self, action: #selector(onDonePressed))
        doneButtonView.addGestureRecognizer(doneTap)

        if let instructions = tripBuilder.currentTrip.instructions{
            if !instructions.isEmpty{
                self.instructionsField.text = instructions
            }
        }

        doneButtonView.round(corners: .allCorners, radius: 5)
        containerView.round(corners: .allCorners, radius: 8)

        instructionsField.becomeFirstResponder()

        self.containerView.alpha = 0
        self.alphaView.alpha = 0
    }

    override func viewDidAppear(_ animated: Bool) {
        self.animateIn()
    }

    func addShadowAndCornerRadius(view: UIView){
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        view.layer.shadowPath = UIBezierPath(roundedRect:
            view.bounds, cornerRadius: view.layer.cornerRadius).cgPath
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: -4, height: 4)
        view.layer.shadowRadius = 10
        view.layer.masksToBounds = true
    }

    @objc func onDonePressed(){
        self.instructionsField.resignFirstResponder()
        if !instructionsField.text!.isEmpty{
            tripBuilder.setInstructions(instructions: instructionsField.text ?? "")
        }
        self.dismissView()
    }

    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }


    func animateIn(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.alphaView.alpha = 0.45
            self.containerView.alpha = 1
        }) { (finished) in

        }
    }

    func animateOut(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.alphaView.alpha = 0
            self.containerView.alpha = 0
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }

}
