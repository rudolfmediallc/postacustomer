//
//  UpdateEmailVC.swift
//  Posta
//
//  Created by Dan Rudolf on 10/17/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class UpdateEmailVC: UIViewController {

    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var emailTextfield: UITextField!

    let userController = UserController.sharedInstace
    let postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
        setTextfields(user: UserController.currentUser)
    }

    func setTextfields(user: User){
        emailTextfield.text = UserController.currentUser.email
        emailTextfield.becomeFirstResponder()
    }

    @IBAction func onCancelPressed(_ sender: Any) {
        self.resignResponders()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onSavePressed(_ sender: Any) {
        self.resignResponders()

        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)

        let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
        loadingView.modalPresentationStyle = .overCurrentContext

        guard var email = emailTextfield.text else{
            banner.show(message: "Please enter your email address")
            return
        }
        email = email.trimmingCharacters(in: .whitespacesAndNewlines)
        email = email.lowercased()
        guard !email.isEmpty else{
            banner.show(message: "Please enter your email address")
            return
        }
        guard email.contains("@") else {
            banner.show(message: "Please enter a valid email address")
            return
        }
        guard email.contains(".") else {
            banner.show(message: "Please enter a valid email address")
            return
        }

        present(loadingView, animated: false, completion: nil)
        userController.updateUsersEmail(email: email) { (success, err) in
            loadingView.dismissDelayed()
            guard err == nil else{
                banner.showDelayed(message: err!.message)
                return
            }
            self.dismissView()
        }
    }

    func dismissView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }

    func resignResponders(){
        guard !emailTextfield.isFirstResponder else{
            emailTextfield.resignFirstResponder()
            return
        }
    }
}
