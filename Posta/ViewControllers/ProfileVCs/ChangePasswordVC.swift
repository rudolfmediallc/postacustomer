//
//  ChangePasswordVC.swift
//  Posta
//
//  Created by Dan Rudolf on 10/17/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {


    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var currentPassTextEdit: UITextField!
    @IBOutlet weak var newPassTextEdit: UITextField!
    @IBOutlet weak var confirmPassTextEdit: UITextField!

    let userController = UserController.sharedInstace
    let postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
        currentPassTextEdit.becomeFirstResponder()
    }

    @IBAction func onCancelPressed(_ sender: Any) {
        self.resignResponders()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onSavePressed(_ sender: Any) {
        self.resignResponders()

        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)

        let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
        loadingView.modalPresentationStyle = .overCurrentContext

        guard let current = currentPassTextEdit.text, !currentPassTextEdit.text!.isEmpty else{
            banner.show(message: "Please enter your current password to make changes.")
            return
        }

        guard !newPassTextEdit.text!.isEmpty, newPassTextEdit.text!.count >= 8 else{
            banner.show(message: "New passwords must be atleast 8 characters long.")
            return
        }

        guard newPassTextEdit.text == confirmPassTextEdit.text else{
            banner.show(message: "New password and confirmation do not match.")
            return
        }

        guard !newPassTextEdit.text!.contains(" ") else {
            banner.show(message: "Passwords cannot contain spaces.")
            return
        }

        present(loadingView, animated: false, completion: nil)
        userController.updateUsersPass(current: current, new: newPassTextEdit.text!) { (success, err) in
            loadingView.dismissDelayed()
            guard err == nil else{
                banner.showDelayed(message: err!.message)
                return
            }
            self.dismissView()
        }
    }

    func dismissView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }

    func resignResponders(){
        self.confirmPassTextEdit.resignFirstResponder()
        self.newPassTextEdit.resignFirstResponder()
        self.currentPassTextEdit.resignFirstResponder()
    }

}

