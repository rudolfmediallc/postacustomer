//
//  UpdatePhoneVC.swift
//  Posta
//
//  Created by Dan Rudolf on 10/17/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class UpdatePhoneVC: UIViewController {

    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!

    let userController = UserController.sharedInstace
    let postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        setTextfields(user: UserController.currentUser)
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
    }

    func setTextfields(user: User){
        phoneTextField.text = UserController.currentUser.phone
        phoneTextField.becomeFirstResponder()
    }

    @IBAction func onCancelPressed(_ sender: Any) {
        self.resignResponders()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onSavePressed(_ sender: Any) {
        self.resignResponders()

        let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
        loadingView.modalPresentationStyle = .overCurrentContext

        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)

        guard var phone = phoneTextField.text else{
            banner.show(message: "Please enter your phone number.")
            return
        }
        phone = phone.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !phone.isEmpty else{
            banner.show(message: "Please enter your phone number.")
            return
        }
        guard phone.count == 10 else {
            banner.show(message: "Please enter a valid phone number omitting country code.")
            return
        }

        present(loadingView, animated: false, completion: nil)
        userController.updateUsersPhone(phone: phone){ (success, err) in
            loadingView.dismissDelayed()
            guard err == nil else{
                banner.showDelayed(message: err!.message)
                return
            }
            self.dismissView()
        }
    }

    func dismissView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }

    func resignResponders(){
        guard !phoneTextField.isFirstResponder else{
            phoneTextField.resignFirstResponder()
            return
        }
    }

}
