//
//  NewTermsVC.swift
//  Posta
//
//  Created by Dan Rudolf on 11/11/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class NewTermsVC: UIViewController {

    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var termsTextView: UITextView!
    @IBOutlet weak var cancelButton: UIButton!

    let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
    let userController = UserController.sharedInstace

    var terms = Terms()

    var agreed: Bool = false{
        didSet{
            checkImageView.image = agreed ? UIImage.init(named: "terms_accepted") : UIImage.init(named: "terms_default")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        termsTextView.text = terms.termsString
        loadingView.modalPresentationStyle = .overCurrentContext
    }

    override func viewDidLayoutSubviews() {
        agreeButton.round(corners: .allCorners, radius: 5)
    }

    @IBAction func onAgreePressed(_ sender: Any) {
        agreed = true
        self.present(loadingView, animated: false, completion: nil)
        userController.agreeToTermsAndConditions(version: terms.version) { (success, err) in
            self.loadingView.dismissDelayed()
            guard err == nil else{
                DispatchQueue.main.async {
                    let banner = BannerView.init(parent: self)
                    banner.setDurationAndLevel(duration: .short, level: .warn)
                    banner.show(message: "Error saving terms signature, please try again.")
                }
                return
            }
            self.proceedAsync()
        }
    }
    @IBAction func onCancelPressed(_ sender: Any) {
        dismissAsync()
    }

    func dismissAsync(){
        DispatchQueue.main.async {
            let vc = self.presentingViewController as! ApplicationStateController
            vc.dismiss(animated: true, completion: nil)
            AuthController.sharedInstance.logout()
            vc.presentLogin()
        }
    }

    func proceedAsync(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            let vc = self.presentingViewController as! ApplicationStateController
            vc.dismiss(animated: true, completion: nil)
            vc.presentApplicationLoader()
        })
    }

}
