//
//  DeliveryHistoryVC.swift
//  Posta
//
//  Created by Dan Rudolf on 6/10/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class DeliveryHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var tripsTableView: UITableView!

    let tripController = TripController.sharedInstace
    let formatter = Formatter()
    let refreshControl = UIRefreshControl()
    let postaTheme = PostaTheme()
    let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)


    var completedTrips: [Trip]!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.presenting = self
        loadingView.modalPresentationStyle = .overCurrentContext
        registerTableview(tableView: self.tripsTableView)
        loadTrips()
    }

    override func viewDidLayoutSubviews() {
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
    }

    @objc func loadTrips(){
        loadingView.show()
        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)
        tripController.getCompletedTrips { (trips, err) in
            self.loadingView.dismissDelayed()
            guard err == nil else{
                banner.showDelayed(message: "Error retreiving your delivery history. Please try again.")
                return
            }
            self.completedTrips = trips
            self.reloadOnMain()
        }
    }

    //MARK: Tableview datasource / delegate
    func registerTableview(tableView: UITableView){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = self.refreshControl
        refreshControl.addTarget(self, action: #selector(loadTrips), for: .valueChanged)
        tableView.register(UINib.init(nibName: "DeliveryHistoryCell", bundle: Bundle.main), forCellReuseIdentifier: "TRIP_CELL")
    }

    func reloadOnMain(){
        DispatchQueue.main.async {
            self.tripsTableView.reloadData()
            if (self.refreshControl.isRefreshing){
                self.refreshControl.endRefreshing()
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 67.0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let trips = self.completedTrips else {
            return 0
        }
        return trips.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TRIP_CELL", for: indexPath) as! DeliveryHistoryCell
        let trip = completedTrips[indexPath.row]
        cell.setDetails(trip: trip)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let destinationVC = PastTripVC.init(nibName: "PastTripVC", bundle: Bundle.main)
            let trip = self.completedTrips[indexPath.row]
            destinationVC.currentTrip = trip
            self.present(destinationVC, animated: true, completion: nil)
        }
    }

    //MARK: Actons and selectors
    @IBAction func onMenuePressed(_ sender: Any) {
        guard let main = self.parent as? MainVC else{
            return
        }
        main.shouledShowMenu(show: true)
    }

}
