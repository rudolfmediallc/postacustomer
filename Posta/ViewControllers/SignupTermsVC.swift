//
//  SignupTermsVC.swift
//  Posta
//
//  Created by Dan Rudolf on 8/24/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class SignupTermsVC: UIViewController {

    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var termsTextView: UITextView!


    override func viewDidLoad() {
        super.viewDidLoad()
        addShadow(view: titleBarView)
        let terms = Terms()
        termsTextView.text = terms.termsString
    }

    @IBAction func onMenuePressed(_ sender: Any) {
        guard let main = self.parent as? MainVC else{
            return
        }
        main.shouledShowMenu(show: true)
    }

    func addShadow(view: UIView){
        var shadowLayer: CAShapeLayer!
        shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 2).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor

        shadowLayer.shadowColor = UIColor.darkGray.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = 2

        view.layer.insertSublayer(shadowLayer, at: 0)
    }
    
    @IBAction func onDonePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
