//
//  ViewController.swift
//  Posta
//
//  Created by Dan Rudolf on 6/7/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit


class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, ProfileSyncDelegate, TripStateDelegate {

    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var menueTableView: UITableView!
    @IBOutlet weak var contentContainer: UIView!
    @IBOutlet weak var menuContainer: UIView!
    @IBOutlet weak var nameLabel: UILabel!

    var menueVisible : Bool = false
    var visibleTab : UIViewController?
    var userController = UserController.sharedInstace
    var tripController = TripController.sharedInstace

    let selectionTint = #colorLiteral(red: 0.09019607843, green: 0.7411764706, blue: 0.8705882353, alpha: 1)

    var homeVC: HomeVC = {
        let vc = HomeVC.init(nibName: "HomeVC", bundle: Bundle.main)
        return vc
    }()

    var profileVC: UserProfileVC = {
        let vc = UserProfileVC.init(nibName: "UserProfileVC", bundle: Bundle.main)
        return vc
    }()

    var currentDeliveriesVC: CurrentDeliveriesVC = {
        let vc = CurrentDeliveriesVC.init(nibName: "CurrentDeliveriesVC", bundle: Bundle.main)
        return vc
    }()

    var paymentVC: PaymentVC = {
        let vc = PaymentVC.init(nibName: "PaymentVC", bundle: Bundle.main)
        return vc
    }()
    var deliveryHistoryVC: DeliveryHistoryVC = {
        let vc = DeliveryHistoryVC.init(nibName: "DeliveryHistoryVC", bundle: Bundle.main)
        return vc
    }()

    var termsVC: TermsVC = {
        let vc = TermsVC.init(nibName: "TermsVC", bundle: Bundle.main)
        return vc
    }()


    override func viewDidLoad() {
        super.viewDidLoad()

        tripController.stateDelegate = self

        menueTableView.dataSource = self
        menueTableView.delegate = self
        menueTableView.register(UINib.init(nibName: "MenueItemCell", bundle: Bundle.main), forCellReuseIdentifier: "MENUE_CELL")
        let dismissTap = UITapGestureRecognizer.init(target: self, action: #selector(dismissMenu))
        dismissTap.cancelsTouchesInView = false
        self.contentContainer.addGestureRecognizer(dismissTap)
        self.setViewControllerAsChildViewController(viewController: homeVC)
        self.logoutButton.layer.cornerRadius = 8
        self.logoutButton.layer.borderColor = UIColor.black.cgColor
        self.logoutButton.layer.borderWidth = 1
        self.logoutButton.layer.masksToBounds = false
        self.nameLabel.text = UserController.currentUser.name.getNameString()
        userController.addDelegate(delegate: self)

        let postaTheme = PostaTheme()
        postaTheme.applyHorizontalDropShadow(view: menuContainer)
        menuContainer.setDropShadow(radius: 3, opacity: 0.25, offset: CGSize.init(width: 1, height: 0))
    }
    override func viewDidLayoutSubviews() {
        self.menuContainer.center = CGPoint.init(x: -(self.menuContainer.frame.width/2), y: self.view.center.y)
    }


    deinit {
        userController.removeDelegate(delegate: self)
    }
    func didUpdateProfileData() {
        self.nameLabel.text = UserController.currentUser.name.getNameString()
    }

    func addShadowAndCornerRadius(view: UIView){
        view.layer.shadowPath = UIBezierPath(roundedRect:
            view.bounds, cornerRadius: view.layer.cornerRadius).cgPath
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: -2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.masksToBounds = false
    }

    private func setViewControllerAsChildViewController(viewController: UIViewController) {
        addChildViewController(viewController)
        contentContainer.addSubview(viewController.view)
        viewController.view.frame = contentContainer.bounds
        viewController.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        viewController.didMove(toParentViewController: self)
        if let childTab = self.visibleTab {
            if childTab != viewController {
                removeViewControllerAsChildViewController(viewController: childTab)
            }
        }
        self.visibleTab = viewController
    }

    private func removeViewControllerAsChildViewController(viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }

    @objc func dismissMenu(){
        if self.menueVisible {
            shouledShowMenu(show: false)
        }
    }

    func shouledShowMenu(show: Bool){
        if show == true{
            self.menueVisible = true
            UIViewPropertyAnimator(duration: 0.2, curve: .easeOut) {
                self.menuContainer.center = CGPoint.init(x: (self.menuContainer.frame.width/2), y: self.view.center.y)
                self.contentContainer.alpha = 0.75
                self.contentContainer.transform = CGAffineTransform(scaleX: 0.92, y: 0.92)
            }.startAnimation()
        } else {
            self.menueVisible = false
            UIViewPropertyAnimator(duration: 0.2, curve: .easeIn) {
                self.menuContainer.center = CGPoint.init(x: -(self.menuContainer.frame.width/2), y: self.view.center.y)
                self.contentContainer.alpha = 1
                self.contentContainer.transform = CGAffineTransform(scaleX: 1, y: 1)
            }.startAnimation()
        }
    }

    @IBAction func onMenuPressed(_ sender: Any) {
        shouledShowMenu(show: true)
    }
    
    @IBAction func onLogoutPRessed(_ sender: Any) {
        UserController.currentUser = nil
        AuthController.sharedInstance.logout()
        AppDelegate.shared.applicationStateController.presentLogin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func onHomeSelected(){
        self.setViewControllerAsChildViewController(viewController: homeVC)
        self.shouledShowMenu(show: false)
    }
    func onProfileSelected(){
        self.setViewControllerAsChildViewController(viewController: profileVC)
        self.shouledShowMenu(show: false)
    }
    func onCurrentDeliveriesSelected(){
        self.setViewControllerAsChildViewController(viewController: currentDeliveriesVC)
        self.shouledShowMenu(show: false)
    }
    func onDeliveryHistorySelected(){
        self.setViewControllerAsChildViewController(viewController: deliveryHistoryVC)
        self.shouledShowMenu(show: false)
    }
    func onPaymentSelected(){
        self.setViewControllerAsChildViewController(viewController: paymentVC)
        self.shouledShowMenu(show: false)
    }
    func onTermsSelected(){
        self.setViewControllerAsChildViewController(viewController: termsVC)
        self.shouledShowMenu(show: false)
    }

    //Trip State Delegate
    func didCreateTrip() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.setAlertImage(alertType: .package)
            alertVC.setAlertTitle(title: "Request Received!")
            alertVC.setAlertMessage(body: "Once we confirm the address with the recipient, we'll dispatch a courier to come get your package")
            self.present(alertVC, animated: false, completion: nil)
        }
    }
    func didCancelTrip() {

    }
    func didUpdateTrip() {

    }

    //MARK:  TableView datasouce/delegate

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? MenueItemCell{
            cell.cellImageView.image = cell.cellImageView.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.cellImageView.tintColor = selectionTint
            cell.cellTitleLabel.textColor = selectionTint
        }
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? MenueItemCell{
            cell.cellImageView.image = cell.cellImageView.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.cellImageView.tintColor = UIColor.black
            cell.cellTitleLabel.textColor = UIColor.black
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MENUE_CELL", for: indexPath) as! MenueItemCell
        switch indexPath.row {
        case 0:
            cell.topDivider.backgroundColor = UIColor.groupTableViewBackground
            cell.cellTitleLabel.text = "Home"
            cell.cellImageView.image = UIImage.init(named: "location_search_icon")
            break
        case 1:
            cell.cellTitleLabel.text = "Current Deliveries"
            cell.cellImageView.image = UIImage.init(named: "current_deliveries")
            break
        case 2:
            cell.cellTitleLabel.text = "Delivery History"
            cell.cellImageView.image = UIImage.init(named: "delivery_history_icon")
            break
        case 3:
            cell.cellTitleLabel.text = "Payment"
            cell.cellImageView.image = UIImage.init(named: "payment_icon_small")
            break
        case 4:
            cell.cellTitleLabel.text = "Settings"
            cell.cellImageView.image = UIImage.init(named: "settings_icon")
            break
        default:
            break
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.onHomeSelected()
            break
        case 1:
            self.onCurrentDeliveriesSelected()
            break
        case 2:
            self.onDeliveryHistorySelected()
            break
        case 3:
            self.onPaymentSelected()
            break
        case 4:
            self.onProfileSelected()
            break
        default:
            break
        }

    }

}

