//
//  ResetPasswordVC.swift
//  Posta
//
//  Created by Dan Rudolf on 12/11/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {

    var lookUpAccountView: LookupAccountView!
    var confirmDeviceView: ConfirmDeviceView!
    var changePasswordView: ChangePasswordView!
    var resetCompleteView: ResetCompleteView!
    var currentView: UIView!

    var deviceToken: String!
    var resetToken: String!
    var phoneNumber: String!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidLayoutSubviews() {
        if lookUpAccountView == nil {
            lookUpAccountView = LookupAccountView.init(frame: self.view.frame)
            lookUpAccountView.parentVC = self
            self.currentView = lookUpAccountView
            self.view.addSubview(lookUpAccountView)
        }
    }

    //MARK: Container Management
    func swapOutView(visibleView: UIView, for View: UIView){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            self.dissolveView(visibleView: visibleView)
            self.alphaInView(newView: View)
            self.currentView = View
        }
    }

    func dissolveView(visibleView: UIView){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.21) {
            visibleView.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.2) {
            visibleView.alpha = 0
        }
    }

    func alphaInView(newView: UIView){
        newView.alpha = 0
        self.view.addSubview(newView)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.21) {
            UIView.animate(withDuration: 0.2) {
                newView.alpha = 1
            }
        }
    }

    func showConfirmDevice(){
        confirmDeviceView = ConfirmDeviceView.init(frame: self.view.frame)
        confirmDeviceView.parentVC = self
        guard let current = currentView as? LookupAccountView else {
            return
        }
        current.animateOut()
        swapOutView(visibleView: current, for: confirmDeviceView)
    }

    func showChangePassword(){
        changePasswordView = ChangePasswordView.init(frame: self.view.frame)
        changePasswordView.parentVC = self
        guard let current = currentView as? ConfirmDeviceView else {
            print(self.currentView)
            return
        }
        current.animateOut()
        swapOutView(visibleView: current, for: changePasswordView)
    }

    func showResetComplete(){
        resetCompleteView = ResetCompleteView.init(frame: self.view.frame)
        resetCompleteView.parentVC = self
        guard let current = currentView as? ChangePasswordView else {
            return
        }
        current.animateOut()
        swapOutView(visibleView: current, for: resetCompleteView)
    }
}
