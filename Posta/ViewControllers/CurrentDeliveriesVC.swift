//
//  CurrentDeliveriesVC.swift
//  Posta
//
//  Created by Dan Rudolf on 6/10/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class CurrentDeliveriesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var tripsTableView: UITableView!

    let tripController = TripController.sharedInstace
    let formatter = Formatter()
    let refreshControl = UIRefreshControl()
    let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
    let postaTheme = PostaTheme()

    var currentTrips: [Trip]!

    override func viewDidLoad() {
        super.viewDidLoad()

        loadingView.presenting = self
        loadingView.modalPresentationStyle = .overCurrentContext

        registerTableview(tableView: self.tripsTableView)
        loadTrips()
    }

    @objc func loadTrips(){
        loadingView.show()
        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)
        tripController.getCurrentTrips { (trips, err) in
            self.loadingView.dismissDelayed()
            guard err == nil else {
                banner.showDelayed(message: "Something went wrong retreiving your current deliveries. Please try again.")
                self.reloadOnMain()
                return
            }
            self.currentTrips = trips
            self.reloadOnMain()
        }
    }

    override func viewDidLayoutSubviews() {
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
    }

    //MARK: Tableview datasource / delegate
    func registerTableview(tableView: UITableView){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = self.refreshControl
        refreshControl.addTarget(self, action: #selector(loadTrips), for: .valueChanged)
        tableView.register(UINib.init(nibName: "CurrentDeliveriesCell", bundle: Bundle.main), forCellReuseIdentifier: "TRIP_CELL")
    }

    func reloadOnMain(){
        DispatchQueue.main.async {
            self.tripsTableView.reloadData()
            if (self.refreshControl.isRefreshing){
                self.refreshControl.endRefreshing()
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 67.0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let trips = self.currentTrips else {
            return 0
        }
        return trips.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TRIP_CELL", for: indexPath) as! CurrentDeliveriesCell
        let trip = currentTrips[indexPath.row]
        cell.dateLabel.text = formatter.getDateAndTime(str: trip.timestamps.created)
        cell.statusLabel.text = trip.getTripStatus()
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let destinationVC = CurrentTripVC.init(nibName: "CurrentTripVC", bundle: Bundle.main)
            let trip = self.currentTrips[indexPath.row]
            destinationVC.currentTrip = trip
            self.present(destinationVC, animated: true, completion: nil)
        }
    }

    //MARK: Actons and selectors
    @IBAction func onMenuePressed(_ sender: Any) {
        guard let main = self.parent as? MainVC else{
            return
        }
        main.shouledShowMenu(show: true)
    }

}
