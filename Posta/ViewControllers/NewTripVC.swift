//
//  NewTripVC.swift
//  Posta
//
//  Created by Dan Rudolf on 10/27/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class NewTripVC: UIViewController, SnappyCardProtocol {

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var mapContainer: UIView!

    var mapController: MapController!
    var tripBuilder = TripBuilder.sharedInstance
    var defaultCard: Card!
    var loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
    var contentView: NewDeliveryView!
    var card: SnappyCard!
    let postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.modalPresentationStyle = .overCurrentContext

        self.mapController = MapController.init(view: mapContainer, parent: self, tracking: .disabled, trip: tripBuilder.currentTrip as AnyObject)
        self.mapController.updateTripData(trip: tripBuilder.currentTrip as AnyObject)
        self.mapController.mapView.setZoomLevel(16, animated: true)


        postaTheme.applyButtonShadow(view: closeButton)
        self.closeButton.layer.cornerRadius = 6

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if contentView == nil {
            contentView = NewDeliveryView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.frame.width, height: 540)), parent: self)
            contentView.parentVC = self
        }

        if card == nil {
            card = SnappyCard.init(parent: self, content: contentView, verticalBound: 210)
            card.delegate = self
            self.view.addSubview(card)
            card.setCollapsed()
            card.shouldScroll = false
        }

    }


    func didExpantCard() { }

    func didCollapseCard() {}

    func presentLoading(){
        self.present(loadingView, animated: false, completion: nil)
    }

    func dismissLoading(){
        self.loadingView.dismissImediate()
    }

    @IBAction func onClosePressed(_ sender: Any) {
        self.tripBuilder.clean()
        self.shouldDismiss()
    }

    func shouldDismiss(){
        self.dismiss(animated: true, completion: nil)
    }

    func dismissNavigationStack(){
        DispatchQueue.main.async {
            let vc = self.presentingViewController as! ApplicationStateController
            vc.dismiss(animated: true, completion: nil)
        }
    }

}

