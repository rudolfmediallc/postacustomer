//
//  AddressSearchVC.swift
//  Posta
//
//  Created by Dan Rudolf on 6/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import MapboxGeocoder

class AddressSearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate, GeoQueryStremDelegate {

    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var isPickupLabel: UILabel!
    @IBOutlet weak var titleBar: UIView!

    let geoController = GeoController.sharedInstace
    let tripBuilder = TripBuilder.sharedInstance
    let addressParser = AddressParser()
    let postaTheme = PostaTheme()

    var currentLocation: CLLocationCoordinate2D!
    var geoResults: [GeoResult] = []
    var isPickupLocation: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        geoController.queryStreamDelegate = self

        searchTextField.delegate = self
        resultsTableView.delegate = self
        resultsTableView.dataSource = self
        resultsTableView.register(UINib.init(nibName: "GeoItemCell", bundle: Bundle.main), forCellReuseIdentifier: "GEO_CELL")

        let dismissTap = UITapGestureRecognizer.init(target: self, action: #selector(resignRresponders))
        dismissTap.delegate = self
        dismissTap.cancelsTouchesInView = false

        self.view.addGestureRecognizer(dismissTap)
        if isPickupLocation {
            isPickupLabel.text = "Where is your item?"
        } else{
            isPickupLabel.text = "Where is your item going?"
        }
        postaTheme.applyDropShadowForTitleBar(view: titleBar)

        let userController = UserController.sharedInstace
        currentLocation = userController.lastLocation
        searchTextField.becomeFirstResponder()
    }

    override func viewWillDisappear(_ animated: Bool) {
        geoController.clearQueryStreamResults()
    }

    @objc func resignRresponders(){
        self.searchTextField.resignFirstResponder()
    }


    //MARK: Tableview Datasource/delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return geoResults.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView.init(frame: CGRect.zero)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let geoItem = geoResults[indexPath.row]

        guard checkBounds(coord: geoItem.coordinate) else {
            return
        }

        isPickupLocation ? tripBuilder.setPickupAddress(newAddress: geoItem.address,
                                                        coordinates: geoItem.coordinate) : tripBuilder.setDestinationAddress(newAddress: geoItem.address,
                                                                                                                             coordinates: geoItem.coordinate)
        self.dismiss(animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GEO_CELL") as! GeoItemCell
        let geoItem = geoResults[indexPath.row]
        cell.primaryLabel.text = geoItem.address.getStreet(type: .full)
        cell.secondaryLabel.text = geoItem.address.getLocaleAddress(type: .full)
        return cell
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        geoController.codeStreamQuery(str: str)
        return true
    }

    func checkBounds(coord: CLLocationCoordinate2D) -> Bool {
        let bounds = Bounds.init()
        guard bounds.checkIsInRegion(point: coord) == true else{
            let outOfRegionAlert = OutOfRegionAlert.init(nibName: "OutOfRegionAlert", bundle: Bundle.main)
            outOfRegionAlert.modalPresentationStyle = .overCurrentContext
            self.present(outOfRegionAlert, animated: false, completion: nil)
            return false
        }
        return true
    }


    //Geo Query Delegate

    func didUpdateStreamResults(geoResults: [GeoResult]) {
        self.geoResults = geoResults
        self.resultsTableView.reloadData()
    }

    func getCoderRegion() -> RectangularRegion{
        let swLat = self.currentLocation.latitude - 0.5
        let swLon = self.currentLocation.longitude - 0.5
        let neLat = self.currentLocation.latitude + 0.5
        let neLon = self.currentLocation.longitude + 0.5

        let southWest = CLLocationCoordinate2D.init(latitude: swLat, longitude: swLon)
        let northEast = CLLocationCoordinate2D.init(latitude: neLat, longitude: neLon)
        return RectangularRegion.init(southWest: southWest, northEast: northEast)
    }

    @IBAction func onCancelPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
