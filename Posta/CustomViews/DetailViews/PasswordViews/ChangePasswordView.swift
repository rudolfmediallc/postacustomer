//
//  ChangePasswordView.swift
//  Posta
//
//  Created by Dan Rudolf on 12/11/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class ChangePasswordView: UIView {

    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var bottomContainer: UIView!
    @IBOutlet weak var middleContainer: UIView!
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet var nibView: UIView!

    let userController = UserController.sharedInstace

    var parentVC: ResetPasswordVC!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("ChangePasswordView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        self.addSubview(content)

        setGestureRecognizers()
    }

    override func layoutSubviews() {
        postRenderView(view: updateButton)
    }

    //MARK: Actions and Selectors
    @IBAction func onCancelPressed(_ sender: UIButton) {
        resignResponders()
        self.parentVC.dismiss(animated: true, completion: nil)
    }

    @IBAction func onUpdatePressed(_ sender: Any) {

        guard checkValidPasswords() == true else{
            return
        }

        let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
        loadingView.modalPresentationStyle = .overCurrentContext
        loadingView.presenting = parentVC
        loadingView.show()

        let password = passwordTextField.text!

        userController.resetPassword(phone: parentVC.phoneNumber, resetKey: parentVC.resetToken, newPassword: password) { (succees, err) in
            loadingView.dismissDelayed()
            guard err == nil else{
                self.showAlertVC(message: err!.message)
                return
            }
            self.presentCompleteView()
        }
    }

    //MARK: Validators
    func checkValidPasswords() -> Bool{
        let banner = BannerView.init(parent: self.parentVC)
        banner.setDurationAndLevel(duration: .short, level: .info)

        guard let pass = passwordTextField.text else{
            banner.show(message: "Please enter a new password. Passwords must be atleast 6 characters long.")
            return false
        }

        guard pass.count > 7 else {
            banner.show(message: "Passwords must be atleast 8 characters long.")
            return false
        }
        guard let confirmPass = newPasswordTextField.text else {
            banner.show(message: "Please enter your password again to confirm.")
            return false
        }
        guard pass == confirmPass else{
            banner.show(message: "Passwords do not match.")
            return false
        }

        return true
    }

    //MARK: View Management
    @objc func animateOut(){
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseIn, animations: {
            self.bottomContainer.center = CGPoint.init(x: -(self.bottomContainer.frame.width/2), y: self.bottomContainer.center.y)
        }) { (done) in }

        UIView.animate(withDuration: 0.4, delay: 0.15, options: .curveEaseIn, animations: {
            self.middleContainer.center = CGPoint.init(x: -(self.middleContainer.frame.width/2), y: self.middleContainer.center.y)
        }) { (done) in }

        UIView.animate(withDuration: 0.4, delay: 0.3, options: .curveEaseIn, animations: {
            self.topContainer.center = CGPoint.init(x: -(self.topContainer.frame.width/2), y: self.topContainer.center.y)
        }) { (done) in }
    }

    func postRenderView(view: UIView){
        let origin = view.frame.origin
        let width = self.frame.width - 32
        let height = view.frame.height
        view.frame = CGRect.init(origin: origin, size: CGSize.init(width: width, height: height))
        view.round(corners: .allCorners, radius: 5)
    }

    @objc func resignResponders(){
        _  = passwordTextField.canResignFirstResponder ? passwordTextField.resignFirstResponder() : nil
        _  = newPasswordTextField.canResignFirstResponder ? newPasswordTextField.resignFirstResponder() : nil
    }

    func showAlertVC(message: String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.setAlertImage(alertType: .fail)
            alertVC.setAlertTitle(title: "Could Not Complete Request")
            alertVC.setAlertMessage(body: message)
            self.parentVC.present(alertVC, animated: false, completion: nil)
        }
    }

    func presentCompleteView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.parentVC.showResetComplete()
        })
    }

    //MARK: Gesture Recognition
    func setGestureRecognizers(){
        let dismissTap = UITapGestureRecognizer.init(target: self, action: #selector(resignResponders))
        self.addGestureRecognizer(dismissTap)
    }

}
