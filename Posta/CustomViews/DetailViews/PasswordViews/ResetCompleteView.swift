//
//  ResetCompleteView.swift
//  Posta
//
//  Created by Dan Rudolf on 12/11/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class ResetCompleteView: UIView {

    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet var nibView: UIView!
    
    var parentVC: ResetPasswordVC!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("ResetCompleteView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        self.addSubview(content)
    }

    override func layoutSubviews() {
        postRenderView(view: self.signInButton)
    }

    //MARK: Actions and Selectors
    @IBAction func onSignInPressed(_ sender: Any) {
        parentVC.dismiss(animated: true, completion: nil)
    }

    //View Rendering
    func postRenderView(view: UIView){
        let origin = view.frame.origin
        let width = self.frame.width - 32
        let height = view.frame.height
        view.frame = CGRect.init(origin: origin, size: CGSize.init(width: width, height: height))
        view.round(corners: .allCorners, radius: 5)
    }

}
