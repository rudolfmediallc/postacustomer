//
//  PastTripDetailsView.swift
//  Posta
//
//  Created by Dan Rudolf on 10/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//


import UIKit

class PastTripDetailsView: UIView {

    @IBOutlet weak var feeDescriptionLabel: UILabel!
    @IBOutlet weak var paymentStatusLabel: UILabel!
    @IBOutlet weak var verticalSeporator: UIView!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var pickupNameLabel: UILabel!
    @IBOutlet weak var pickupStreetLabel: UILabel!
    @IBOutlet weak var desNameLabel: UILabel!
    @IBOutlet weak var desStreetLabel: UILabel!
    @IBOutlet weak var pickupDot: UIView!
    @IBOutlet weak var dropoffDot: UIView!
    @IBOutlet weak var deliveryIdLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pickupTimeLabel: UILabel!
    @IBOutlet weak var dropoffTimeLabel: UILabel!
    @IBOutlet weak var paymentContainer: UIView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var fareLabel: UILabel!
    @IBOutlet weak var distanceTimeLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardImageVIew: UIImageView!
    @IBOutlet var nibView: UIView!
    @IBOutlet weak var horizontalSeporator: UIView!

    var trip: Trip!

    let formatter = Formatter()
    let postaTheme = PostaTheme()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("PastTripDetailsView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }

        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        self.addSubview(content)
    }

    override func layoutSubviews() {
        pickupDot.setCircleView()
        dropoffDot.setCircleView()
        postaTheme.fillHorizontalGradient(view: paymentContainer)
        postaTheme.fillHorizontalGradient(view: horizontalSeporator)
        postaTheme.fillVerticalGradient(view: verticalSeporator)

        paymentContainer.round(corners: .allCorners, radius: 8)
    }

    func populateDetailsForTripState(trip: Trip){
        self.trip = trip
        switch trip.status {
        case 4:
            populateDelivered()
            break
        case 5:
            populateCanceled()
            break
        default:
            break
        }
    }

    func populateCanceled(){

        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        pickupTimeLabel.text = "Pickup"
        dropoffTimeLabel.text = "Drop off"

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)
        let total = formatter.getCurrencyValue(val: (trip.transaction.amount.total))

        if trip.transaction.amount.total == 0{
            paymentStatusLabel.text = "CLOSED"
        }

        fareLabel.text = total
        totalLabel.text = total
        feeDescriptionLabel.text = "Cancelation Fee"
        cardImageVIew.image = getCardImage(brand: trip.transaction.charge.cardType)
        cardNumberLabel.text = "\u{2022}\u{2022}\u{2022}\(trip.transaction.charge.last4)"
        distanceTimeLabel.text = formatter.getElapsedTimeDistanceString(trip: trip)

        statusImageView.image = UIImage.init(named: "canceled_delivery_icon")

    }

    func populateDelivered(){
        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        pickupTimeLabel.text = formatter.getTime(str: (trip.timestamps.received)!)
        dropoffTimeLabel.text = formatter.getTime(str: (trip.timestamps.delivered)!)

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)
        let total = formatter.getCurrencyValue(val: (trip.transaction.amount.total))

        fareLabel.text = total
        totalLabel.text = total
        cardImageVIew.image = getCardImage(brand: trip.transaction.charge.cardType)
        cardNumberLabel.text = "\u{2022}\u{2022}\u{2022}\(trip.transaction.charge.last4)"
        distanceTimeLabel.text = formatter.getElapsedTimeDistanceString(trip: trip)

        statusImageView.image = UIImage.init(named: "delivered_icon")
    }


    func getCardImage(brand: String) -> UIImage{
        switch brand {
        case "American Express":
            return UIImage.init(named: "amex_small")!
        case "Diners Club":
            return UIImage.init(named: "dc_small")!
        case "Discover":
            return UIImage.init(named: "discover_small")!
        case "JCB":
            return UIImage.init(named: "jcb_small")!
        case "MasterCard":
            return UIImage.init(named: "master_card_small")!
        case "UnionPay":
            return UIImage.init(named: "union_pay_small")!
        case "Visa":
            return UIImage.init(named: "visa_small")!
        default:
            return UIImage.init(named: "default_card_small")!
        }
    }
}
