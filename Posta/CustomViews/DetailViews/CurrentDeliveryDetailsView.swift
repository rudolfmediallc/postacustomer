//
//  CurrentDeliveryDetailsView.swift
//  Posta
//
//  Created by Dan Rudolf on 10/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class CurrentDeliveryDetailsView: UIView, OptionsControllerDelegate {

    @IBOutlet weak var verticalSpacer: UIView!
    @IBOutlet weak var receivedLabelSmall: UILabel!
    @IBOutlet weak var pickupNameLabel: UILabel!
    @IBOutlet weak var pickupStreetLabel: UILabel!
    @IBOutlet weak var desNameLabel: UILabel!
    @IBOutlet weak var desStreetLabel: UILabel!
    @IBOutlet weak var pickupDot: UIView!
    @IBOutlet weak var dropoffDot: UIView!
    @IBOutlet weak var deliveredLabelSmall: UILabel!
    @IBOutlet weak var deliveryIdLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pickupTimeLabel: UILabel!
    @IBOutlet weak var dropoffTimeLabel: UILabel!
    @IBOutlet weak var courierNameLabel: UILabel!
    @IBOutlet weak var courierDistanceLabel: UILabel!
    @IBOutlet weak var arivingShortlyLabel: UILabel!
    @IBOutlet var nibView: UIView!
    @IBAction func optionsButton(_ sender: Any) {
    }
    @IBOutlet weak var optionsButton: UIButton!


    var parentVC: UIViewController!
    var trip: Trip!

    let formatter = Formatter()
    let postaTheme = PostaTheme()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("CurrentDeliveryDetailsView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }

        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
    }

    override func layoutSubviews() {
        pickupDot.setCircleView()
        dropoffDot.setCircleView()
        postaTheme.fillHorizontalGradient(view: optionsButton)
        postaTheme.fillVerticalGradient(view: verticalSpacer)

        optionsButton.round(corners: .allCorners, radius: 5)
    }

    //MARK Options Menu Delegate
    func didSelectCallCourier() {
        self.trip.courier?.phone.makeCall()
    }

    func didSelectCallRecipient() {
        self.trip.destination.phone.makeCall()
    }

    func didSelectCancelTrip() {
        let tripController = TripController.sharedInstace
        tripController.updateTripStatus(trip_id: trip.id, status: 5, confirmCode: nil) { (trip, err) in
            guard err == nil else{
                DispatchQueue.main.async {
                    let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
                    alertVC.modalPresentationStyle = .overCurrentContext
                    alertVC.setAlertImage(alertType: .fail)
                    alertVC.setAlertTitle(title: "Trip Cancelation Failed")
                    alertVC.setAlertMessage(body: err!.message)
                    self.parentVC.present(alertVC, animated: false, completion: nil)
                }
                return
            }
            DispatchQueue.main.async {
                self.populateTripDetailsForState(trip: trip!)
            }
        }
    }


    //MARK Actions and Selectors
    @IBAction func onOptonsPressed(_ sender: UIButton) {
        DispatchQueue.main.async {
            let optionsVC = OptionsViewController.init(nibName: "OptionsViewController", bundle: Bundle.main)
            optionsVC.currentTrip = self.trip
            optionsVC.delegate = self
            optionsVC.modalPresentationStyle = .overCurrentContext
            self.parentVC.present(optionsVC, animated: true, completion: nil)
        }
    }


    //MARK View Rendering and Data Population

    func populateTripDetailsForState(trip: Trip){
        self.trip = trip
        switch (trip.status){
        case 0:
            populateNew()
            break
        case 1:
            populatConfirmd()
            break
        case 2:
            populateCourierAccepted()
            break
        case 3:
            populateReceived()
            break
        case 4:
            populateDelivered()
            break
        case 5:
            populateCancled()
            break
        default:
            break
        }

    }

    func populateNew(){
        deliveredLabelSmall.isHidden = true
        receivedLabelSmall.isHidden = true

        pickupTimeLabel.text = "Pickup"
        dropoffTimeLabel.text = "Drop off"

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

    }

    func populatConfirmd(){
        deliveredLabelSmall.isHidden = true
        receivedLabelSmall.isHidden = true

        pickupTimeLabel.text = "Pickup"
        dropoffTimeLabel.text = "Drop off"

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)

        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

        courierNameLabel.text = "Dispatching Shortly"
        courierDistanceLabel.text = ""

    }

    func populateCourierAccepted(){
        deliveredLabelSmall.isHidden = true
        receivedLabelSmall.isHidden = true

        pickupTimeLabel.text = "Pickup"
        dropoffTimeLabel.text = "Drop off"

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

        guard let courier = trip.courier else {
            courierNameLabel.text = "Contacting Nearby Couriers"
            courierDistanceLabel.text = "Dispatching Shortly..."
            return
        }

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)

        arivingShortlyLabel.text = "Courier Dispatched"
        courierNameLabel.text = courier.name.getNameAndInitial()

        let distance = formatter.getDistancbetween(current: trip.pickupGeo.getCoordinates(), des: courier.location.getCoordinates())

        courierDistanceLabel.text = "\(distance)mi Away"
    }

    func populateReceived(){
        deliveredLabelSmall.isHidden = true
        receivedLabelSmall.isHidden = false

        pickupTimeLabel.text = formatter.getTime(str: trip.timestamps.received!)
        dropoffTimeLabel.text = "Drop off"

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

        guard let courier = trip.courier else {
            courierNameLabel.text = "Contacting Nearby Couriers"
            courierDistanceLabel.text = "Dispatching Shortly..."
            return
        }

        arivingShortlyLabel.text = "Out For Delivery"
        courierNameLabel.text = courier.name.getNameAndInitial()

        let distance = formatter.getDistancbetween(current: courier.location.getCoordinates(), des: trip.destintationGeo.getCoordinates())

        courierDistanceLabel.text = "\(distance)mi Away"
    }

    func populateDelivered(){

        deliveredLabelSmall.isHidden = true
        receivedLabelSmall.isHidden = true

        pickupTimeLabel.text = formatter.getTime(str: trip.timestamps.received!)
        dropoffTimeLabel.text = formatter.getTime(str: trip.timestamps.delivered!)

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)


        guard let courier = trip.courier else {
            courierNameLabel.text = "Contacting Nearby Couriers"
            courierDistanceLabel.text = "Dispatching Shortly..."
            return
        }

        arivingShortlyLabel.text = "Delivery Completed"
        courierNameLabel.text = courier.name.getNameAndInitial()

        courierDistanceLabel.text = formatter.getElapsedTimeDistanceString(trip: trip)
    }

    func populateCancled(){

        deliveredLabelSmall.isHidden = true
        receivedLabelSmall.isHidden = true

        pickupTimeLabel.text = "Pickup"
        dropoffTimeLabel.text = "Drop off"

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)

        guard let courier = trip.courier else {
            courierNameLabel.text = "Not Assigned"
            courierDistanceLabel.text = "Order Canceled "
            return
        }

        arivingShortlyLabel.text = "Trip Complete"
        courierNameLabel.text = courier.name.getNameAndInitial()
        courierDistanceLabel.text = "Order Canceled"
    }

}

