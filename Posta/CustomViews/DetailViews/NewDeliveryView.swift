//
//  NewDeliveryView.swift
//  Posta
//
//  Created by Dan Rudolf on 10/27/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import CoreLocation

class NewDeliveryView: UIView, TripBuilderDelegate {
    
    @IBOutlet weak var frameWidth: NSLayoutConstraint!

    @IBOutlet weak var pickupNameLabel: UILabel!
    @IBOutlet var nibView: UIView!
    @IBOutlet weak var pickupAddressLabel: UILabel!
    @IBOutlet weak var destinationNameLabel: UILabel!
    @IBOutlet weak var destinationAddressLabel: UILabel!
    @IBOutlet weak var destinationPhoneLabel: UILabel!
    @IBOutlet weak var editDestinationButton: UIButton!
    @IBOutlet weak var editInstructionsButton: UIButton!
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var priceContainer: UIView!
    @IBOutlet weak var requestCourierButton: UIButton!
    @IBOutlet weak var pickupDot: UIView!
    @IBOutlet weak var dropffDot: UIView!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var crediCardImage: UIImageView!
    @IBOutlet weak var last4Label: UILabel!
    @IBOutlet weak var setDestinationButton: UIButton!
    @IBOutlet weak var deliverLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!


    let tripBuilder = TripBuilder.sharedInstance
    let userController = UserController.sharedInstace
    let tripController = TripController.sharedInstace
    let postaTheme = PostaTheme()

    var parentVC: NewTripVC?

    init(frame: CGRect, parent: NewTripVC) {
        super.init(frame: frame)
        parentVC = parent
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("NewDeliveryView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }
        
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        editDestinationButton.isHidden = true
        setupForDestinationInput(hasDestination: false)
        populateDetails(tripRequest: tripBuilder.currentTrip)

        tripBuilder.builderDelegate = self
        self.addSubview(content)
    }


    override func layoutSubviews() {
        editDestinationButton.round(corners: .allCorners, radius: 3)
        editInstructionsButton.round(corners: .allCorners, radius: 3)
        setDestinationButton.round(corners: .allCorners, radius: 5)
        priceContainer.round(corners: .allCorners, radius: 5)
        requestCourierButton.round(corners: .allCorners, radius: 5)
        crediCardImage.round(corners: .allCorners, radius: 5)
        crediCardImage.layer.masksToBounds = true

        postaTheme.fillHorizontalGradient(view: priceContainer)
        postaTheme.fillHorizontalGradient(view: requestCourierButton)

        pickupDot.setCircleView()
        dropffDot.setCircleView()

        postaTheme.fillVerticalGradient(view: seperatorView)
    }

    func setupForDestinationInput(hasDestination: Bool){
        self.dropffDot.isHidden = !hasDestination
        self.deliverLabel.isHidden = !hasDestination
        self.seperatorView.isHidden = !hasDestination
        self.editDestinationButton.isHidden = !hasDestination
        setDestinationButton.isHidden = hasDestination
    }


    @IBAction func onEditDestinationPressed(_ sender: Any) {
        showAddressCoder()
    }

    @IBAction func onEditInstructionsPressed(_ sender: Any) {
        showInstructionsEntry()
    }


    @IBAction func onRequestCourier(_ sender: Any) {
        let coder = JSONCoder()
        let tripJSON = coder.encodeJSON(codable: tripBuilder.currentTrip)
        guard tripJSON != nil else{
            return
        }
        self.parentVC!.presentLoading()
        tripController.submitTrip(trip: tripJSON!) { (trip, err) in
            self.parentVC?.dismissLoading()
            guard err == nil else{
                return
            }
            self.tripBuilder.clean()
            self.parentVC?.dismissNavigationStack()
        }
    }

    // Trip Builder Delegate
    func didSetDropoff(tripRequest: TripRequest) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.showNameEntry()
        }
    }

    func didSetDetails(tripRequest: TripRequest) {
        destinationNameLabel.text = tripRequest.destination?.name?.getNameString()
        destinationPhoneLabel.text = tripRequest.destination?.phone
        destinationAddressLabel.text = tripRequest.destination?.address.getStreet(type: .full)

        parentVC!.mapController.updateTripData(trip: tripBuilder.currentTrip as AnyObject)
        parentVC?.mapController.mapView.setZoomLevel(16, animated: true)

        setupForDestinationInput(hasDestination: true)
        parentVC!.card.updateVerticalBound(newBound: 245)
        parentVC!.card.shouldScroll = true
        parentVC!.card.setExpanded()
    }

    func didSetInstructions(tripRequest: TripRequest) {
        instructionsLabel.text = tripRequest.instructions
    }

    func didSetPickup(address: Address, coordinates: CLLocationCoordinate2D) {}

    func populateDetails(tripRequest: TripRequest){
        pickupNameLabel.text = UserController.currentUser.profile.name.getNameString()
        pickupAddressLabel.text = UserController.currentUser.profile.address.getStreet(type: .full)
        let card = parentVC!.defaultCard
        last4Label.text = "\u{2022}\u{2022}\u{2022}\(card!.lastFour)"
        crediCardImage.image = card!.getCardImage()
        crediCardImage.round(corners: .allCorners, radius: 3)
    }

    func showAddressCoder(){
        let destinationVC = AddressSearchVC.init(nibName: "AddressSearchVC", bundle: Bundle.main)
        destinationVC.isPickupLocation = false
        parentVC!.present(destinationVC, animated: true, completion: nil)
    }

    func showNameEntry(){
        let destinationVC = NameVC.init(nibName: "NameVC", bundle: Bundle.main)
        destinationVC.modalPresentationStyle = .overCurrentContext
        parentVC!.present(destinationVC, animated: false, completion: nil)
    }
    
    func showInstructionsEntry(){
        let destinationVC = InstructionsVC.init(nibName: "InstructionsVC", bundle: Bundle.main)
        destinationVC.modalPresentationStyle = .overCurrentContext
        parentVC!.present(destinationVC, animated: false, completion: nil)
    }


}
