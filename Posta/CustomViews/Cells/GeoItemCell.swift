//
//  GeoItemCell.swift
//  Posta
//
//  Created by Dan Rudolf on 6/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class GeoItemCell: UITableViewCell {

    @IBOutlet weak var primaryLabel: UILabel!
    @IBOutlet weak var secondaryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
