//
//  DeliveryHistoryCell.swift
//  Posta
//
//  Created by Dan Rudolf on 7/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class DeliveryHistoryCell: UITableViewCell {

    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!

    var formatter = Formatter()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setDetails(trip: Trip){
        iconImageView.image = trip.status == 4 ? UIImage.init(named: "delivered_icon") : UIImage.init(named: "canceled_delivery_icon")
        dateLabel.text = formatter.getDateAndTime(str: trip.timestamps.updated)
        statusLabel.text = formatter.getElapsedTimeDistanceString(trip: trip)
        costLabel.text = formatter.getCurrencyValue(val: trip.transaction.amount.total)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
