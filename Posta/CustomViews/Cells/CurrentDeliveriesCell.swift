//
//  CurrentDeliveriesCell.swift
//  Posta
//
//  Created by Dan Rudolf on 7/6/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class CurrentDeliveriesCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    
}
