//
//  PostaTheme.swift
//  Posta
//
//  Created by Dan Rudolf on 11/16/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation
import UIKit

class PostaTheme: NSObject {

    var mainTeal = #colorLiteral(red: 0.1294117647, green: 0.831372549, blue: 0.8862745098, alpha: 1)

    var mainPurple = #colorLiteral(red: 0.4039215686, green: 0.5215686275, blue: 0.9215686275, alpha: 1)

    var buttonBlue = #colorLiteral(red: 0.1450980392, green: 0.5411764706, blue: 0.8705882353, alpha: 1)

    var accentGrey = #colorLiteral(red: 0.6588235294, green: 0.7058823529, blue: 0.7450980392, alpha: 1)

    var defaultText = #colorLiteral(red: 0.2196078431, green: 0.2705882353, blue: 0.3098039216, alpha: 1)

    var darkFill = #colorLiteral(red: 0.2196078431, green: 0.2705882353, blue: 0.3098039216, alpha: 1)


    func applyDownwardDropShadow(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 3
    }

    func applyHorizontalDropShadow(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 4.0, height: 0.0)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 3
    }

    func applyBannerDropShadow(view: UIView){
        view.layer.cornerRadius = 8
        let shadowPath2 = UIBezierPath(rect: view.bounds)
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        view.layer.shadowOpacity = 0.4
        view.layer.shadowRadius = 5
        view.layer.shadowPath = shadowPath2.cgPath
    }

    func applyDropShadowForTitleBar(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 1
    }

    func applyUpwardDropShadow(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: -4.0)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 3
    }

    func applyButtonShadow(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 2
    }

    func fillVerticalGradient(view: UIView){
        let gradientView = UIView(frame: CGRect.init(origin: view.frame.origin, size: view.frame.size))
        let gradient = CAGradientLayer()
        gradient.frame = gradientView.bounds
        gradient.colors = [mainTeal.cgColor, mainPurple.cgColor]
        gradient.startPoint = CGPoint.init(x: 0.5, y: 0)
        gradient.endPoint = CGPoint.init(x: 0.5, y: 1)
        view.layer.insertSublayer(gradient, at: 0)
    }

    func fillDiagonalGradient(view: UIView){
        let gradientView = UIView(frame: CGRect.init(origin: view.frame.origin, size: view.frame.size))
        let gradient = CAGradientLayer()
        gradient.frame = gradientView.bounds
        gradient.colors = [mainTeal.cgColor, mainPurple.cgColor]
        gradient.startPoint = CGPoint.init(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint.init(x: 1.0, y: 1.0)
        view.layer.insertSublayer(gradient, at: 0)
    }

    func fillHorizontalGradient(view: UIView){
        let gradientView = UIView(frame: CGRect.init(origin: view.frame.origin, size: view.frame.size))
        let gradient = CAGradientLayer()
        gradient.frame = gradientView.bounds
        gradient.colors = [mainTeal.cgColor, mainPurple.cgColor]
        gradient.startPoint = CGPoint.init(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint.init(x: 1, y: 0.5)
        view.layer.insertSublayer(gradient, below: view.layer.sublayers?.first)
    }

}
