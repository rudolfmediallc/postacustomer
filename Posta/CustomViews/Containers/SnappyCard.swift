//
//  SnappyCard.swift
//  Posta
//
//  Created by Dan Rudolf on 10/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

protocol SnappyCardProtocol {
    func didExpantCard()
    func didCollapseCard()
}


class SnappyCard: UIView {

    var parentVC: UIViewController!
    var verticalBound: CGFloat!
    var contentView: UIView!
    var openCenterAnchor: CGPoint!
    var closedCenterAnchor: CGPoint!
    var indicatorImageView: UIImageView!
    var delegate: SnappyCardProtocol?
    var shouldScroll: Bool = true

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    //Initialize view
    init(parent: UIViewController, content: UIView, verticalBound: CGFloat) {
        let halfedHeight = content.frame.height/2
        let d = halfedHeight - verticalBound
        super.init(frame: CGRect.init(origin: CGPoint.init(x: 0, y: parent.view.frame.maxY - content.frame.height), size: content.frame.size))

        self.verticalBound = verticalBound
        self.parentVC = parent
        self.contentView = content
        self.openCenterAnchor = CGPoint.init(x: parent.view.center.x,
                                             y: parent.view.frame.maxY - halfedHeight - parent.view.safeAreaInsets.bottom)
        self.closedCenterAnchor = CGPoint(x: parent.view.center.x, y: parent.view.frame.maxY + d - parent.view.safeAreaInsets.bottom)

        initView(contentView: self.contentView)
    }

    //Bind View To Parent
    private func initView(contentView: UIView) {
        self.addSubview(contentView)
        self.layoutIfNeeded()
        self.backgroundColor = UIColor.white
        let swipeUp = UISwipeGestureRecognizer.init(target: self, action: #selector(SnappyCard.didPan))
        let swipeDown = UISwipeGestureRecognizer.init(target: self, action: #selector(SnappyCard.didPan))

        swipeUp.direction = [.up]
        swipeDown.direction = [.down]
        self.addGestureRecognizer(swipeUp)
        self.addGestureRecognizer(swipeDown)

        roundViewCorners(view: self)
        setDropShadow(view: self)

        indicatorImageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 26, height: 13))
        indicatorImageView.center = CGPoint.init(x: self.center.x, y: 14)
        indicatorImageView.image = UIImage.init(named: "swipe_icon")
        indicatorImageView.contentMode = .scaleAspectFit
        self.addSubview(indicatorImageView)
        self.bringSubview(toFront: indicatorImageView)
    }

    //MARK: Actions
    @objc private func didPan(_ sender: UISwipeGestureRecognizer){
        if sender.direction == UISwipeGestureRecognizerDirection.up{
            animateOpen()
        } else{
            animateClosed()
        }
    }

    func setExpanded(){
        animateOpen()
    }

    func setCollapsed(){
        animateClosed()
    }


    // Check and hide subviews that will be visible beyond the vertical bound in a colapsed state
    func hideSubviews(){
        let cutoff = verticalBound - safeAreaInsets.bottom
        let subviews = contentView.subviews[0].subviews[0].subviews

        for v in subviews{
            hideIfNeeded(view: v, boundry: cutoff)
        }
    }

    func showSubviews(){
        let subviews = contentView.subviews[0].subviews[0].subviews
        for v in subviews{
            showIfNeeded(view: v)
        }
    }

    func showIfNeeded(view: UIView){
        if (view.alpha == 0.0) {
            view.alpha = 1.0
        }
    }

    func hideIfNeeded(view: UIView, boundry: CGFloat){
        if view.frame.minY > boundry{
            view.alpha = 0.0
        }
    }

    func updateVerticalBound(newBound: CGFloat){
        self.verticalBound = newBound
        let halfedHeight = self.frame.height/2
        let d = halfedHeight - verticalBound
        self.closedCenterAnchor = CGPoint(x: parentVC.view.center.x, y: parentVC.view.frame.maxY + d)
        self.animateToNewBound()
    }


    //MARK: Animations and View Rendering
    private func animateOpen(){
        guard shouldScroll == true else {
            return
        }
        let indcatorTransform = CGAffineTransform.init(rotationAngle: .pi)
        self.delegate?.didExpantCard()
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.center = self.openCenterAnchor
            self.indicatorImageView.transform = indcatorTransform
            self.indicatorImageView.transform = .identity
            self.showSubviews()
        }, completion: nil)
    }

    private func animateClosed(){
        guard shouldScroll == true else {
            return
        }
        let indcatorTransform = CGAffineTransform.init(rotationAngle: .pi)
        self.delegate?.didCollapseCard()
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.center = self.closedCenterAnchor
            self.indicatorImageView.transform = indcatorTransform
            self.hideSubviews()
        }, completion: nil)
    }

    private func animateToNewBound(){
        let indcatorTransform = CGAffineTransform.init(rotationAngle: .pi)
        self.delegate?.didCollapseCard()
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.center = self.closedCenterAnchor
            self.indicatorImageView.transform = indcatorTransform
            self.hideSubviews()
        }, completion: nil)
    }

    func roundViewCorners(view: UIView){
        view.layer.cornerRadius = 12
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    func setDropShadow(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: -5.0)
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 3
    }

}
