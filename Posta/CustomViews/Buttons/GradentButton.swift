//
//  GradentButton.swift
//  Posta
//
//  Created by Dan Rudolf on 10/23/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class GradientButton: UIButton{

    var gradientImageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.reversesTitleShadowWhenHighlighted = false
        self.showsTouchWhenHighlighted = false
//        self.titleLabel?.textColor = UIColor.white
        setShadowAndRadius()
        setGradientBackground()
    }

    override var isHighlighted: Bool {
        didSet {
            alpha = isHighlighted ? 0.8 : 1.0
            gradientImageView.isHidden = isHighlighted
            titleLabel?.textColor = isHighlighted ? UIColor.black : UIColor.white
        }
    }

    private func setGradientBackground(){
        gradientImageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: frame.width, height: frame.height))
        gradientImageView.image = UIImage.init(named: "gradient_fill2")
        gradientImageView.round(corners: .allCorners, radius: layer.cornerRadius)
        gradientImageView.contentMode = .scaleAspectFill
        gradientImageView.layer.masksToBounds = true

        self.addSubview(gradientImageView)
    }

    private func setShadowAndRadius(){
        layer.cornerRadius = 6
//        layer.shadowRadius = 2
//        layer.shadowOpacity = 0.4
//        layer.shadowColor = UIColor.black.cgColor
//        layer.shadowOffset = CGSize(width: 0, height: 1)
//        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        layer.masksToBounds = false
    }
}
