//
//  CloseButton.swift
//  Posta
//
//  Created by Dan Rudolf on 11/16/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class CloseButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = 6
        let imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        imageView.image = UIImage.init(named: "close_icon")
        imageView.center = self.center
        imageView.contentMode = .scaleAspectFit

        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        layer.shadowOpacity = 0.45
        layer.shadowRadius = 2

        self.addSubview(imageView)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setButtonShadow(view: UIView){

    }

}
