//
//  Highlightbutton.swift
//  Posta
//
//  Created by Dan Rudolf on 10/17/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class Highlightbutton: UIButton {

    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? UIColor.groupTableViewBackground : UIColor.white
        }
    }

}
