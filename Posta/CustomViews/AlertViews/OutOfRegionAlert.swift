//
//  OutOfRegionAlert.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 11/14/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class OutOfRegionAlert: UIViewController {

    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var alertImageView: UIImageView!
    @IBOutlet weak var alertTitleLabel: UILabel!
    @IBOutlet weak var alertMessageLabel: UILabel!
    @IBOutlet weak var alertButton: UIButton!
    @IBOutlet weak var alertContainer: UIView!
    @IBOutlet weak var cancelButtonn: UIButton!
    
    var alertTitle: String!
    var displayIcon: UIImage!
    var shouldShrink: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setFramesTransparent(isTransparent: true)
        setDropShadows()
        roundViews()

        alertTitleLabel.text = "Address Unserviceable"
        alertMessageLabel.text = "Your requested address is outside of our current delivery region. View our current service area below"
        alertImageView.image = self.displayIcon
        displayIcon = UIImage.init(named: "map_region_icon")
        alertImageView.image = displayIcon
    }

    override func viewDidAppear(_ animated: Bool) {
        self.animateIn()
    }

    func setFramesTransparent(isTransparent: Bool){
        self.alphaView.alpha = isTransparent ? 0.0 : 0.45
        self.alertContainer.alpha = isTransparent ? 0.0 : 1.0
    }

    func roundViews(){
        self.alertContainer.round(corners: .allCorners, radius: 8)
        self.alertButton.round(corners: .allCorners, radius: 5)
        self.cancelButtonn.round(corners: .allCorners, radius: 5)
    }

    func setDropShadows(){
        alertContainer.setDropShadow(radius: 8.0, opacity: 0.45, offset: CGSize.init(width: 0, height: 1))
    }

    @IBAction func onAlertButtonPressed(_ sender: Any) {
        let serviceAreaVC = ServiceAreaVC.init(nibName: "ServiceAreaVC", bundle: Bundle.main)
        self.present(serviceAreaVC, animated: true, completion: nil)

    }
    @IBAction func onCancelPressed(_ sender: Any) {
        animateOut()
    }
    
    func animateOut(){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.setFramesTransparent(isTransparent: true)
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }

    func animateIn(){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.setFramesTransparent(isTransparent: false)
        }) { (finished) in

        }
    }


}
