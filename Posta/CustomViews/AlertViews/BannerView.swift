//
//  BannerView.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 7/28/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class BannerView: UIView {

    @IBOutlet var nibView: UIView!
    @IBOutlet weak var bannerTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!

    let postaTheme = PostaTheme()

    var parentVC: UIViewController!
    var visibleDuration: Double!

    enum Duration {
        case short
        case medium
        case long
    }

    enum AlertLevel {
        case info
        case warn
    }


    init(parent: UIViewController) {
        let size = CGSize.init(width: (parent.view.frame.width - 16), height: 72)
        super.init(frame: CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: size))
        self.parentVC = parent
        self.visibleDuration = 4.0
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("BannerView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }

        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
        postaTheme.applyBannerDropShadow(view: content)
    }

    func bindBannerViewToParent(){
        parentVC.view.addSubview(self)
        self.center = CGPoint.init(x: parentVC.view.center.x, y: -50)
    }

    func removeBannerViewFromParent(){
        self.removeFromSuperview()
    }

    func setDurationAndLevel(duration: Duration, level: AlertLevel){

        switch level {
        case .warn:
            imageView.image = UIImage.init(named: "warn_icon")
            break
        case .info:
            imageView.image = UIImage.init(named: "info_icon")
            break
        }

        switch (duration) {
        case .short:
            visibleDuration = 4.0
            break
        case .medium:
            visibleDuration = 6.0
            break
        case .long:
            visibleDuration = 8.0
            break
        }
    }

    func show(message: String){
        DispatchQueue.main.async {
            self.bindBannerViewToParent()
            self.bannerTitle.text = message
            self.beginAnimations()
        }
    }

    func showDelayed(message: String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
            self.bindBannerViewToParent()
            self.bannerTitle.text = message
            self.beginAnimations()
        }
    }

    func beginAnimations(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.center = CGPoint.init(x: self.parentVC.view.center.x, y: self.parentVC.view.safeAreaInsets.top + 58)
        }) { (finished) in
            self.hideBanner(animationLength: self.visibleDuration)
        }
    }

    func hideBanner(animationLength: Double){
        UIView.animate(withDuration: 0.2, delay: self.visibleDuration, options: .curveEaseIn, animations: {
            self.center = CGPoint.init(x: self.parentVC.view.center.x, y: -50)
        }) { (finished) in
            self.removeBannerViewFromParent()
        }
    }
}
